\subsection{Prophecy RA}\label{index:pry-la}

The prophecy RA \(\PRY\) is defined as follows.\footnote{\
  Here, in place of \(\PryVar \tofine -\)
  we can use \(\PryTyId \to \NN \tofine -\),
  which can be easier to implement.
}
\begin{gather*}
  \abs{\PRY} \defeq
    \abs{\EX(\PryRes)} \+\X\+
    (\PryAsn \to \Prop) \+\X\+
    \abs{\+ \PryVar \tofine \FRAC \+} \br[.3em]
  (a, \phi, f) \cdot (b, \psi, g) \defeq
    (a \cdot b,\, \lam{\pi}\+ \phi(\pi) \!\!\cand\!\? \psi(\pi),\, f \cdot g) \br[.1em]
  \eps \!\defeq\! (\eps,\+ \lam{\_}\! \top,\+ \eps) \qqquad
  \abs{(a, \phi, f)} \!\defeq\! (\eps, \phi, \eps) \br[.1em]
  \begin{aligned}
  &
    \ok\bigl((a, \phi, f)\bigr) \defeq
      (a = \eps \cand \ex{\pi} \phi(\pi)) \ \cor {} \\[-.2em]
  & \hspace{2em}
      \ex{\+\eP \st a = \exn \eP}\
        \ok(\eP) \cand
        (\all{\pi \st \flr{\eP}(\pi)}\, \phi(\pi)) \cand
        (\all{x \in \pvars(\eP)}\, f(x) = 0)
  \end{aligned}
\end{gather*}
We have up to one prophecy resolution \(\eP\) at the exclusive RA.
In order for the item \((\exn \eP, \phi, f)\) to be valid,
(i) \(\eP\) should be valid,
(ii) \(\phi(\pi)\) should hold for any \(\pi\) satisfying \(\flr{\eP}(\pi)\), and
(iii) \(f(x)\) should be \(0\) for any \(x\) already resolved by \(\eP\).
In order for the item \((\eps, \phi, f)\) to be valid,
\(\phi\) should be satisfiable.
Note that the monotonicity over the validity predicate holds
because of \cref{lemma:Sat-PryRes} \nameref{lemma:Sat-PryRes}.

Assume that the global camera has the prophecies RA as a component.
We use some namespace \(\iN_\pry\) and some ghost name \(\gamma_\pry\).
We introduce the following propositions.
\begin{gather*}
  \Ctx_\pry \,\defeq\, \inv{\ex{\eP}\, \res{
    (\exn \eP,\, \lam{\_}\! \top,\, \eps)}{\gamma_\pry}{\PRY}
  }{\iN_\pry} \qqquad
  \ang{\phi} \defeq
    \res{(\eps, \phi, \eps)}{\gamma_\pry}{\PRY}
  \quad \note{\phi \col \PryAsn \to \Prop} \br[.3em]
  [x]_q \defeq
    \res{(\eps,\ \lam{\_}\! \top,\ [ x \by q ])}{\gamma_\pry}{\PRY}
  \quad \note{q \col \QQpf} \qqquad
  [X]_q \defeq \slim
    \bigsep_{x \in X} [x]_q
  \quad \note{X \col \powfin \PryVar}
\end{gather*}
The context \(\Ctx_\pry\) owns the prophecy resolution \(\eP\) but does not care what \(\eP\) is.
The resource \(\know{\phi}\), which we call a prophecy assertion, observes that \(\phi(\pi)\) holds for any \(\pi\) satisfying the constraint \(\flr{\eP}\).
The resource \([x]_q\) observes with the fraction \(q\) that the prophecy variable \(x\) is not yet resolved;
we use this notation only for \(q\) satisfying \(0 < q \le 1\).

The ghost name \(\gamma_\pry\) is determined at the time we obtain \(\Ctx_\pry\), using the following lemma.
\begin{equation*}
  \yields\!\! \upd\! \ex{\gamma_\pry}\ \Ctx_\pry
  \newrule{PryCtx-Intro}
\end{equation*}

The following properties hold.
\begin{gather*}
  \persistent(\Ctx_\pry) \qqquad
  \persistent\bigl(\know{\phi}\bigr) \qqquad
  \timeless\bigl(\know{\phi}\bigr) \br[.3em]
  \timeless\bigl([x]_q\bigr) \qqquad
  [x]_{q + q'} \biyields [x]_q \sep [x]_{q'} \br[.3em]
  \yields\!\! \upd\?
    \ex{x \st x.\tidn = \tid}\ \
    [x]_1
  \newrule{PryVar-Intro} \br[.3em]
  \Dep(v,\+ Y) \,\imp\,
  [x]_1 \,\sep\,
  [Y]_q
    \ \,\sep\! \Ctx_\pry
    \!\!\yields\!\!
      \updf{\iN_\pry}\,\
    \know{\lam{\pi} \pi(x) = v(\pi)} \,\sep\,
    [Y]_q
  \newrule{PryVar-Resolve} \br[.3em]
  \know{\phi} \,\sep\,
  \know{\psi} \,\yields\,
    \know{\lam{\pi} \phi(\pi) \!\!\cand\!\? \psi(\pi)}
  \newrule{PryAstn-Merge} \br[.3em]
  (\all{\pi}\, \phi(\pi) \!\imp\! \psi(\pi)) \ \imp\
    \know{\phi} \yields \know{\psi}
  \newrule{PryAstn-Weaken} \br[.3em]
  \know{\phi}
    \ \yields\
    \ex{\pi} \phi(\pi)
  \newrule{PryAstn-Sat}
\end{gather*}
We can get the full token \([x]_1\)
for a fresh prophecy variable \(x\) of the specified type id
(\cref{rule:PryVar-Intro}).
By consuming the full token \([x]_1\),
we can resolve \(x\) to a value \(x\),
with the help of the partial token \([Y]_q\) on the prophecy variables that the value \(v\) depends on
(\cref{rule:PryVar-Resolve}).
Prophecy assertions can be merged (\cref{rule:PryAstn-Merge})
and weakened (\cref{rule:PryAstn-Weaken}).
When we have a prophecy assertion \(\know{\phi}\),
we know that \(\phi\) is satisfiable
(\cref{rule:PryAstn-Sat}).

\begin{proof}[Proof of \cref{rule:PryVar-Intro}]
  We check the global frame of \(\PRY\) (at \(\gamma_\PRY\)),
  and pick up a fresh prophecy variable \(x\) (of a specified type id)
  that is neither resolved by the prophecy resolution at the exclusive RA
  and nor in the support of the finite-support map RA over \(\PryVar\).
\end{proof}

\begin{proof}[Proof of \cref{rule:PryVar-Resolve}]
  We update the prophecy resolution,
  owned by the invariant of \(\Ctx_\pry\),
  from \(\eP\) to \(\mapupd{\eP}{x}{v}\).
  We consume the full token \([x]_1\)
  to change the state of the prophecy variable \(x\) from being unresolved to being resolved.
  The partial token \([Y]_q\) working as a catalyst
  ensures that the prophecy variables in \(Y\) has not been resolved.
\end{proof}

\begin{proof}[Proof of \cref{rule:PryAstn-Sat}]
  By the validity predicate of \(\PRY\),
  which itself is justified by \cref{lemma:Sat-PryRes} \nameref{lemma:Sat-PryRes}.
\end{proof}

Also, we introduce the following proposition called a resolver \(\Resolver(w, v)\) (where \(w, v \col \PryAsn \abr\to T\) for some type \(T\)).
\begin{equation*}
  \Resolver(w, v) \,\defeq\,
    \all{Y \st \Dep(v, Y)}\,
    \all{q}\,
    [Y]_q
    \,\wandupdf{\iN_\pry}\,
    \know{\lam{\pi} w_\pi = v_\pi} \,\sep\,
    [Y]_q
\end{equation*}
With a resolver \(\Resolver(x, v)\),
we can get a prophecy assertion \(\know{\lam{\pi} w_\pi = v_\pi}\),
with the help of a partial prophecy token on the prophecy variables that \(v\) depends on.
By definition, we can use a resolver in the following way.
\begin{equation*}
  \Dep(v, Y) \,\imp\,
    \Resolver(w, v) \,\sep\,
    [Y]_q
    \,\yields\!\!
      \updf{\iN_\pry}\,
    \know{\lam{\pi} w_\pi = v_\pi} \,\sep\,
    [Y]_q
  \newrule{Resolver-ResolveUp}
\end{equation*}
We can construct a resolver using the following lemmas.
\begin{gather*}
  [x]_1
    \ \,\sep\! \Ctx_\pry
    \!\!\yields\,\
    \Resolver(\lam{\pi}\? \pi(x),\, v)
  \newrule{PryToken-Resolver} \br[.2em]
  \know{\lam{\pi} w_\pi = v_\pi}
    \ \,\yields\,\
    \Resolver(w, v)
  \newrule{PryAstn-Resolver} \br[.2em]
  \know{\lam{\pi} w'_\pi = w_\pi} \,\sep\,
  \Resolver(w, v)
    \ \yields\
    \Resolver(w', v)
  \newrule{Resolver-Modify} \br[.2em]
  \begin{aligned}
  & \slim
    \bigsep_{i < n} \Resolver(w_i, v_i)
      \ \yields\
      \Resolver(
        \lam{\pi}\? f(w_0(\pi), \ldots, w_{n-1}(\pi)),\,
        \lam{\pi}\? f(v_0(\pi), \ldots, v_{n-1}(\pi))) \\[-.4em]
  & \hspace{1em}
      \note{f\ \text{is injective}}
  \end{aligned} \br[-1.2em]
  \newrule{Resolver-Transform}
\end{gather*}

\begin{proof}[Proof of \cref{rule:PryToken-Resolver}]
  By \cref{rule:PryVar-Resolve}.
\end{proof}

\begin{proof}[Proof of \cref{rule:PryAstn-Resolver}]
  Just by combining \(\know{\lam{\pi} w_\pi = v_\pi}\) with \([Y]_q \wandupdf{\iN_\pry} [Y]_q\).
\end{proof}

\begin{proof}[Proof of \cref{rule:Resolver-Modify}]
  By \(\know{\lam{\pi} w'_\pi = w_\pi} \sep \know{\lam{\pi} w_\pi = v_\pi} \yields \know{\lam{\pi} w'_\pi = v_\pi}\).
\end{proof}

\begin{proof}[Proof of \cref{rule:Resolver-Transform}]
  Let \(w'\) be \(\lam{\pi}\? f(w_0(\pi),\abr \ldots,\abr w_{n-1}(\pi))\) and \(v'\) be \(\lam{\pi}\? f(v_0(\pi),\abr \ldots,\abr v_{n-1}(\pi))\).
  Take any \(Y\) satisfying \(\Dep(v', Y)\) and any \(q\).
  By \cref{rule:Dep-Decompose}, \(\Dep(v_i, Y)\) holds for any \(i < n\).
  So for each \(i\), out of the revolver \(\Resolver(w_i, v_i)\),
  we get \([Y]_{q/n} \,\wandupdf{\iN_\pry} \know{\lam{\pi} w_i(\pi) = v_i(\pi)} \sep [Y]_{q/n}\).\footnote{\
    In the case \(n = 0\), the lemma is clear since we can freely get \(\know{\lam{\pi} f() = f()}\).
    So we can assume \(n > 0\).
  }
  By composing those view shifts in parallel,
  and using the fact that \(\bigsep_i\, \know{\lam{\pi} w_i(\pi) = v_i(\pi)}\) entails \(\know{\lam{\pi} w'_\pi = v'_\pi}\),
  we get \([Y]_q \,\wandupdf{\iN_\pry} \know{\lam{\pi} w'(\pi) = v'(\pi)} \sep [Y]_q\).
\end{proof}
