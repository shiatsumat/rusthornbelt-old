From iris.proofmode Require Import tactics.
From iris.base_logic Require Export na_invariants.
From lrust.prophecy Require Export base.
From lrust.lang Require Export proofmode notation.
From lrust.lifetime Require Export lifetime.
From lrust.typing Require Export unqref.

Class typeG Σ := TypeG {
  type_prophG :> prophG Σ;
  type_unqrefG :> unqrefG Σ;
  type_heapG :> lrustG Σ;
  type_lftG :> lftG Σ;
  type_na_invG :> na_invG Σ;
}.

Definition thread_id := na_inv_pool_name.

Definition inc_list κ κl `{!typeG Σ} : iProp Σ :=
    [∗ list] κ' ∈ κl, (κ ⊑ κ')%I.
Notation "κ ⊑+ κl" := (inc_list κ κl)
  (at level 70, format "κ  ⊑+  κl") : bi_scope.

Record type `{!typeG Σ} A := {
  ty_lfts : list lft;
  ty_own : thread_id → (proph_asn → A) → nat → list val → iProp Σ;
  ty_shr : lft → thread_id → loc → (proph_asn → A) → nat → iProp Σ;

  ty_shr_persistent κ tid l pv d : Persistent (ty_shr κ tid l pv d);
  ty_own_mono_dep tid pv d d' vl :
    d ≤ d' → ty_own tid pv d vl -∗ ty_own tid pv d' vl;
  ty_shr_mono_dep κ tid l pv d d' :
    d ≤ d' → ty_shr κ tid l pv d -∗ ty_shr κ tid l pv d';
  ty_shr_mono_lft κ κ' tid l pv d :
    κ' ⊑ κ -∗ ty_shr κ tid l pv d -∗ ty_shr κ' tid l pv d;
  ty_share E κ l tid pv d q : ↑lftN ⊆ E →
    lft_ctx -∗ &{κ} (l ↦∗: ty_own tid pv d) -∗ q.[κ] ={E}=∗
    ty_shr κ tid l pv d ∗ q.[κ];
  (* ... *)
}.
