\subsection{Basic Connectives of Iris}\label{index:iris-base}

We introduce basic logical connectives on Iris propositions.
Mainly because of step indexing, the definitions are rather hard to understand;
we recommend to read the definitions ignoring the step indexing at first.

\paragraph{Sequent}

The sequent on Iris propositions is defined as follows.
\begin{equation*}
  \vwith{P}{
    P \yields Q \,\defeq\,
      \all{n,\, a \st \Ok{n}(a)}\,
      \step{n}{P}(a) \!\!\imp\!\!
      \step{n}{Q}(a)
  }
\end{equation*}
We write \(\yields\! P\) for \(\yields\! P\) (\(\top\) is introduced later).
We write \(P \!\biyields\! Q\) for \(P \!\yields\! Q \cand Q \!\yields\! P\).
The binary relation \(\yields\) forms a partial order over \(\IProp\), i.e. the following properties hold.\footnote{\
  We need propositional extensionality \((\phi \!\!\iff\!\! \psi) \!\iff\! \phi = \psi\)
  to prove the last property \(P \!\biyields\! Q \!\iff\! P = Q\).
}
\begin{equation*}
  P \yields P \qqquad
  P \!\yields\! Q \ \cand\
  Q \!\yields\! R \ \imp\
    P \!\yields\! R \qqquad
  P \biyields Q \ \iff\ P = Q
\end{equation*}

\paragraph{Operator precedence on connectives}

We write down the operator precedence on the connectives here beforehand:
(i, the strongest) \(\pers\), \(\later\);
(ii) \(\sep\), \(\land\);
(iii) \(\lor\);
(iv) \(\wand\), \(\limp\);
(v, the weakest) \(\upd\), \(\all{x}\), \(\ex{x}\).

\subsubsection{Connectives of intuitionistic logic}

We introduce connectives of intuitionistic logic over \(\IProp\) as follows.
\begin{gather*}
  \top \col \IProp \,\defeq\, \lam{\_} \lam{\_} \top \qqquad
  \bot \col \IProp \,\defeq\, \lam{\_} \lam{\_} \bot \br[.0em]
  P \land Q \ \defeq\
    \lam{a}\ \lam{n}\
    \step{n}{P}(a) \cand \step{n}{Q}(a) \qqquad
  \all{x \col T} P_x \ \defeq\
    \lam{a}\ \lam{n}\
    \all{x \col T} \step{n}{P_x}(a) \br[-.1em]
  P \limp Q \ \defeq\
    \lam{a}\ \lam{n}\
    \all{m \le n,\ b \st a \?\itemle\? b \!\cand\! \Ok{m}(b)}\
    \step{m}{P}(b) \imp \step{m}{Q}(b) \br[-.1em]
  P \lor Q \ \defeq\
    \lam{a}\ \lam{n}\
    \step{n}{P}(a) \cor \step{n}{Q}(a) \qqquad
  \ex{x \col T} P_x \ \defeq\
    \lam{a}\ \lam{n}\
    \ex{x \col T} \step{n}{P_x}(a)
\end{gather*}
If we ignore step indexing, the definition above turns out to follow the standard Kripke semantics for intuitionistic logic (each element of a camera behaves like a possible world).
As expected,
the truth, the falsehood, the conjunction, the implication and the disjunction defined above
semantically work as
the maximum, the minimum, the supremum, the relative pseudo-complement and the infimum on the partial order of the sequent;
in other words, the following properties hold.
\begin{gather*}
  P \yields \top \qqquad
  \bot \yields P \br[.1em]
  R \yields P \land Q \ \iff\
    R \!\yields\! P \ \cand\
    R \!\yields\! Q \qqquad
  Q \yields (\all{x} P_x) \ \iff\
    \all{x}\, (Q \!\yields\! P_x) \br[.1em]
  P \land Q \yields R \ \iff\
    P \yields (Q \limp R) \br[.1em]
  P \lor Q \yields R \ \iff\
    P \!\yields\! R \ \cand\
    Q \!\yields\! R \qqquad
  (\ex{x} P_x) \yields Q \ \iff\
    \all{x}\, (P_x \!\yields\! Q)
\end{gather*}
Therefore, tautologies in intuitionistic logic hold also in Iris.

Also, a pure proposition \(\phi \col \Prop\) can be promoted into an Iris proposition as follows.
\begin{equation*}
  \ceil{\phi} \,\defeq\,
    \lam{\_} \lam{\_} \phi
\end{equation*}
The following properties hold.
\begin{gather*}
  \ceil{\phi} \,\land\, P \yields Q \ \iff\
    (\phi \!\imp\! (P \!\yields\! Q)) \qqquad
  (\phi \!\imp\! \psi) \iff
    \ceil{\phi} \yields \ceil{\psi} \br[.1em]
  \ceil{\top} \biyields \top \qqquad
  \ceil{\bot} \biyields \bot \qqquad
  \ceil{\phi \land \psi} \biyields
    \ceil{\phi} \,\land\, \ceil{\psi} \qqquad
  \ceil{\all{x} \phi_x} \biyields
    \all{x} \ceil{\phi_x} \br[.1em]
  \ceil{\phi \!\imp\! \psi} \biyields
    \ceil{\phi} \,\limp\, \ceil{\psi} \qqquad
  \ceil{\phi \lor \psi} \biyields
    \ceil{\phi} \,\lor\, \ceil{\psi} \qqquad
  \ceil{\ex{x} \phi_x} \biyields
    \ex{x} \ceil{\phi_x}
\end{gather*}
Hereafter, we simply write \(\phi\) for \(\ceil{\phi}\).

We introduce the following shorthand.
\begin{equation*}
  P \liff Q \,\defeq\,
    (P \limp Q) \,\land\, (Q \limp P) \qqquad
  \ex{x \st \phi_x}\, P \,\defeq\,
    \ex{x}\, \phi_x \land P
\end{equation*}

\subsubsection{Separating conjunction and magic wand}

The separating conjunction is defined as follows.
\begin{equation*}
  \vwith{P}{
    P \sep Q \ \defeq\
    \lam{a}\ \lam{n}\
    \ex{b \st \step{n}{P}(b),\ c \st \step{n}{Q}(c)}\
    a \Eq{n} b \cdot c
  }
\end{equation*}
It means that we have a resource \(a\) that decomposes into \(b\) and \(c\), which respectively satisfies \(P\) and \(Q\).
The following properties hold.
\begin{gather*}
  (P \sep Q) \sep R \biyields P \sep (Q \sep R) \qqquad
  P \sep Q \biyields Q \sep P \qqquad
  P \sep \top \biyields P \br[.1em]
  P \!\yields\! Q \ \cand\
  P' \!\yields\! Q' \ \imp\
    P \sep P' \yields Q \sep Q'
\end{gather*}
The first, second, third properties show that
the separating conjunction forms a commutative monoid (having \(\top\) for the unit element).
The fourth property says that we can perform parallel composition of sequents using the separating conjunction.
Note that we can derive the following properties.
\begin{equation*}
  P \sep Q \yields P \qquad
  P \sep Q \yields P \land Q \qquad
  P \,\sep\, \all{x} Q_x \,\yields\,
    \all{x}\, P \sep Q_x \qquad
  P \yields Q \imp P \sep R \yields Q \sep R
\end{equation*}

We can construct the right adjoint \(P \wand Q\) of the separating conjunction \(P \sep Q\), which is called the magic wand.
It is defined as follows.
\begin{equation*}
  \vwith{P}{
    P \wand Q \ \defeq\
      \lam{a}\ \lam{n}\
      \all{m \le n,\ b \st \Ok{m}(a \cdot b)}\
      \step{m}{P}(b) \imp \step{m}{Q}(a \cdot b)
  }
\end{equation*}
It means that we have a resource \(a\) such that,
for any valid resource \(b\) satisfying \(P\),
\(a \cdot b\) satisfies \(Q\).
It is in fact the right adjoint of the separating conjunction, i.e. the following holds.
\begin{equation*}
  P \sep Q \yields R \ \iff\
    P \yields Q \wand R
\end{equation*}
Also, the following properties hold.
\begin{gather*}
  P \limp Q \,\yields\, P \wand Q \qqquad
  P \,\sep\, (P \wand Q) \yields Q \qqquad
  (P \wand Q) \,\sep\, (Q \wand R) \,\yields\, P \wand R \br[.1em]
  P \sep Q \wand R \biyields
    P \wand (Q \wand R) \qqquad
  \yields P \wand P \qqquad
  (P \wand P') \,\sep\,
  (Q \wand Q') \,\yields\,
    P \sep Q \wand P' \sep Q'
\end{gather*}

With the help of the magic wand, we can derive the following property on the separating conjunction.
\begin{equation*}
  P \,\sep\, \ex{x} Q_x \,\biyields\,
    \ex{x}\, P \sep Q_x
\end{equation*}
We use \(\ex{x} Q_x \yields P \,\wand\, \ex{x}\, P \sep Q_x\) as a lemma.
It is an instance of `left adjoints preserve colimits'.

\subsubsection{Persistence modality}

The persistence modality \(\pers\) is defined as follows.
\begin{equation*}
  \vwith{P}{
    \pers P \ \defeq\
      \lam{a}\ \lam{n}\
      \step{n}{P}(\abs{a})
  }
\end{equation*}
It means that we can satisfy \(P\) only by the core of the resource we have.
We have the following basic properties on the persistence modality.
\begin{equation*}
  P \yields Q \ \imp\
    \pers P \yields \pers Q \qqquad
  \pers P \yields P \qqquad
  \pers \pers P \biyields \pers P \qqquad
  Q \,\land\, \pers P \biyields Q \,\sep\, \pers P
\end{equation*}
The persistence modality is monotone and monadic.
The fourth rule says that the plain conjunction can be strengthen into the separating conjunction when one operand is under the persistence modality.
The persistence modality satisfies the following commutativity properties.
\begin{gather*}
  \pers P \,\land\, \pers Q \biyields
  \pers\, (P \land Q) \biyields
  \pers\, (P \sep Q) \biyields
    \pers P \,\sep\, \pers Q \qqquad
  \pers\, (\all{x} P_x) \biyields
    \all{x} \pers P_x \br[.1em]
  \pers\, (P \lor Q) \biyields
    \pers P \,\lor\, \pers Q \qqquad
  \pers\, (\ex{x} P_x) \biyields
    \ex{x} \pers P_x \br[.1em]
  \pers\, (P \wand Q) \biyields
  \pers\, (P \limp Q) \yields
  \pers P \limp \pers Q \biyields
  \pers P \wand \pers Q \qqquad
  \phi \biyields \pers \phi
\end{gather*}

Using the persistence modality,
we define the property that an Iris proposition \(P\) is persistent as follows.
\begin{equation*}
  \persistent(P) \,\defeq\,
    P \yields \pers P
\end{equation*}
It is equivalent to \(P \biyields \pers P\)
(since \(\pers P \yields P\) always hold)
and also to \(\all{n,\, a \st \Ok{n}(a)}\, \step{n}{P}(a) = \step{n}{P}(\abs{a})\),
meaning that \(P\) is using only the core of the resource it has.
Persistent propositions satisfy the following properties.
\begin{gather*}
  \persistent(P) \,\imp\,
    P \land Q \biyields P \sep Q \qqquad
  \persistent(P) \,\imp\,
    P \wand Q \biyields P \limp Q \br[.1em]
  \persistent(P) \,\cand\, P \yields Q \,\imp\,
    P \yields \pers Q \qqquad
  \persistent(P) \,\imp\,
    P \biyields P \sep P \br[.3em]
  \persistent(Q) \,\cand\,
  P \yields Q \,\imp\,
    P \yields P \sep Q
\end{gather*}
We have the following properties on judging persistent propositions.
\begin{gather*}
  \persistent(\pers P) \qqquad
  \persistent(\phi) \qqquad \br[.1em]
  \persistent(P) \imp
    \persistent(Q \limp P),\
    \persistent(Q \wand P) \br[.1em]
  \persistent(P) \cand \persistent(Q) \imp
    \persistent(P \land Q),\
    \persistent(P \lor Q),\
    \persistent(P \sep Q) \br[.1em]
  (\all{x}\, \persistent(P_x)) \imp
    \persistent(\all{x} P_x),\ \persistent(\ex{x} P_x)
\end{gather*}

\subsubsection{Update modality}

We have the update modality \(\upd\), which is defined as follows.
\begin{equation*}
  \upd P \ \defeq\
    \lam{a}\ \lam{n}\
    \all{m \le n,\ c \st \Ok{m}(a \cdot c)}\
    \ex{b \st \Ok{m}(b \cdot c)}\
    \step{m}{P}(b)
\end{equation*}
It means that we have a resource \(a\)
and that, whatever the remaining part \(c\) of the global resource is,
we can find a resource \(b\) satisfying \(P\) such that
we can safely update \(a\) into \(b\).
The update modality satisfies the following properties.
\begin{equation*}
  P \!\yields\! Q \,\imp\,
    \upd P \!\yields\! \upd Q \qqquad
  P \yields \upd P \qqquad
  \upd\! \upd P \biyields \upd P \qqquad
  P \,\sep\, \upd Q \,\yields\, \upd (P \sep Q)
\end{equation*}
The update modality is monotone (the first property) and monadic (the second and third property).
The fourth property says that we can move the (separately conjoined) external resource \(P\) inside the update modality,
which can be regarded as the frame property.
The update modality satisfies the following (weak) commutativity properties.
\begin{gather*}
  \upd P \,\sep\, \upd Q
    \,\yields\, \upd (P \sep Q) \qqquad
  \upd P \,\lor\, \upd Q
    \,\yields\, \upd (P \lor Q) \qqquad
  \ex{x} \upd P_x
    \,\yields\, \upd \ex{x} P_x
\end{gather*}

\subsubsection{Connecting Iris propositions and cameras}

We introduce the primitive connective for owning an item of the global camera \(a \col \abs{\GlCmra}\).
\begin{equation*}
  \Own(a) \ \defeq\
    \lam{b}\ \lam{n}\
    a \Itemle{n} b
\end{equation*}
Note that we use the step-indexed inclusion \(a \Itemle{n} b\) here.
The following properties hold.
\begin{gather*}
  \Own(a \cdot b) \biyields
    \Own(a) \sep \Own(b) \qqquad
  \yields \Own(\eps) \qqquad
  a = \abs{a} \imp
    \persistent(\Own(a)) \br[.1em]
  a \itemupd B \,\sep\,
  \Own(a)
    \ \yields\!\! \upd\! \ex{b \!\in\! B}\,\
    \Own(b)
\end{gather*}
The product of the global camera corresponds to the separating conjunction.
The resource of owning the unit element \(\eps\) is equivalent to \(\top\).
The resource of owning a core element is persistent.
When we own \(a\) such that the frame-preserving update \(a \itemupd B\) holds,
under the update modality we can own \(b\) for some \(b \in B\).

We use the following shorthand
for \(\cidx \col \CmraIdx\), \(a \col \abs{\GlCmra_\cidx}\), \(\gamma \col \GhName\).
\begin{equation*}
  \res{a}{\gamma}{\cidx} \ \defeq\
    \Own(\mapone{\cidx}{\mapone{\gamma}{a}})
\end{equation*}
It means that we own a resource \(a\) at the ghost state \(\gamma\) at the camera of the camera index \(\cidx\).
The following properties hold.
\begin{gather*}
  \res{a \cdot b}{\gamma}{\cidx} \biyields
    \res{a}{\gamma}{\cidx} \sep
    \res{b}{\gamma}{\cidx} \qqquad
  \yields \res{\eps}{\gamma}{\cidx} \qqquad
  a = \abs{a} \imp
    \persistent(\res{a}{\gamma}{\cidx}) \br[.1em]
  a \itemupd B \,\sep\,
  \res{a}{\gamma}{\cidx}
    \ \yields\!\! \upd\! \ex{b \?\in\? B}\ \
    \res{b}{\gamma}{\cidx}
\end{gather*}
For a camera \(\Cmra\) registered to the global camera
such that \(\Cmra = \GlCmra_\cidx\) holds,
we often write \(\res{a}{\gamma}{\Cmra}\) for \(\res{a}{\gamma}{\cidx}\),
because usually we can uniquely determine \(\cidx\) from \(\Cmra\).

Also, we introduce the connective for the step-indexed validity, which is defined as follows.
\begin{equation*}
  \okd(a) \ \defeq\
    \lam{\_}\ \lam{n}\
    \Ok{n}(a)
    \quad \note{a \col \abs{\Cmra} \ \text{for some camera \(\Cmra\)}} \br[.0em]
\end{equation*}
The following properties hold.
\begin{equation*}
  \persistent(\okd(a)) \qqquad
  \text{\(a\) comes from a discrete camera} \ \imp\
    \okd(a) \biyields \ok(a)
\end{equation*}
Also, we have the following properties on owning a resource and the step-indexed validity.
\begin{gather*}
  \okd(a)
    \ \yields\!\! \upd\! \ex{\gamma}\,\
    \Own(a) \qqquad
  \Own(a) \yields
    \okd(a) \br[.0em]
  \okd(a)
    \ \yields\!\! \upd\! \ex{\gamma}\,\
    \res{a}{\gamma}{\cidx} \qqquad
  \res{a}{\gamma}{\cidx} \yields
    \okd(a)
\end{gather*}
When a resource \(a\) is valid, then we can update the global resource and own \(a\) at some fresh ghost state \(\gamma\).
When we have a resource \(a\) at some ghost state, then we know that \(a\) is valid.

\subsubsection{Later modality}

We introduce the following later modality.
\begin{equation*}
  \vwith{P}{
    \later P \ \defeq\
      \lam{a}\ \lam{n}\
      n = 0 \cor
      \!\step{n-1}{P}\?(a)
  }
\end{equation*}
It means that \(P\) will be satisfied at the next step.
The following properties hold.
\begin{equation*}
  P \yields Q \ \imp\
    \later P \yields \later Q \qqquad
  P \yields \later P
\end{equation*}
The later modality satisfies the following commutativity properties.
\begin{gather*}
  \later\, (P \land Q) \biyields
    \later P \,\land\, \later Q \qqquad
  \later\, (\all{x} P_x) \biyields
    \all{x} \later P_x \qqquad
  \later\, (P \limp Q) \yields
    \later P \limp \later Q \br[.1em]
  \later\, (P \lor Q) \biyields
    \later P \,\lor\, \later Q \qqquad
  \text{\(T\) is inhabited} \ \imp\
  \later\, (\ex{x \col T} P_x) \biyields
    \ex{x \col T} \later P_x \br[.1em]
  \later \top \biyields \top \qqquad
  \later\, (P \sep Q) \biyields
    \later P \,\sep\, \later Q \qqquad
  \later\, (P \wand Q) \yields
    \later P \wand \later Q \qqquad
  \later \pers P \biyields \pers \later P
\end{gather*}
The update modality and the later modality do not commute.
The later modality satisfies the following.
\begin{equation*}
  \persistent(P) \ \imp\
    \persistent(\later P)
\end{equation*}

We have the following theorem for stripping off the update and later modalities from a pure proposition.
\begin{theorem}
  For any pure proposition \(\phi\),
  \(\yields\! (\upd \later)^n \phi\) entails \(\phi\).
\end{theorem}

To aid deduction over the later modality, we introduce the following except-0 modality.
\begin{equation*}
  \exzero P \,\defeq\,
    \later \bot \,\lor\, P
\end{equation*}
It is equivalent to \(\lam{a} \lam{n}\, n = 0 \?\cor\? \step{n}{P}(a)\),
i.e. \(P\) with the step \(0\) masked by \(\top\).
The following properties hold.
\begin{gather*}
  P \yields Q \ \imp\
    \exzero P \yields \exzero Q \qqquad
  P \yields \exzero P \qqquad
  \exzero \exzero P \biyields \exzero P \qqquad
  \exzero \later P \biyields \later P \br[.1em]
  \exzero\, (P \land Q) \biyields
    \exzero P \land \exzero Q \qqquad
    \exzero\, (\all{x} P_x) \biyields
    \all{x} \exzero P_x \qqquad
  \exzero\, (P \limp Q) \yields
    \exzero P \limp \exzero Q \br[.1em]
  \exzero\, (P \lor Q) \biyields
    \exzero P \lor \exzero Q \qqquad
  \exzero\, (\ex{x} P_x) \biyields
    \ex{x} \exzero P_x \br[.1em]
  \exzero\, (P \sep Q) \biyields
    \exzero P \sep \exzero Q \qqquad
  \exzero\, (P \wand Q) \yields
    \exzero P \wand \exzero Q \qqquad
  \exzero \pers P \biyields \pers \exzero P \qqquad
  \exzero \upd P \yields \upd \exzero P
\end{gather*}

Using the later and except-0 modalities,
we define the property that an Iris proposition \(P\) is timeless as follows.
\begin{equation*}
  \timeless(P) \,\defeq\,
    \later P \yields \exzero P
\end{equation*}
It is equivalent to \(\later P \biyields \exzero P\) (since \(\exzero P \yields \later P\) always hold)
and also \(\all{n,\, a \st \Ok{n}(a)}\, \step{0}{P}(a) = \step{n}{P}(a)\),
i.e. \(P\) is constant with respect to the step-index.
Timeless propositions satisfy the following properties.
\begin{equation*}
  \timeless(P) \,\cand\,
  P \yields \exzero Q \,\imp\,
    \later P \yields \exzero Q \qqquad
  \timeless(P) \,\cand\,
  P \yields \later Q \,\imp\,
    \later P \yields \later Q
\end{equation*}
We have the following properties for judging timeless propositions.
\begin{gather*}
  \timeless(\phi) \qqquad
  \timeless(P) \imp
    \timeless(Q \limp P),\
    \timeless(Q \wand P),\
    \timeless(\pers P) \br[.1em]
  \timeless(P) \cand \timeless(Q) \imp
    \timeless(P \land Q),\
    \timeless(P \lor Q),\
    \timeless(P \sep Q) \br[.1em]
  (\all{x} \timeless(P_x)) \imp
    \timeless(\all{x} P_x),\ \timeless(\ex{x} P_x) \br[-.1em]
  \text{\(a\) comes from a discrete camera} \imp
    \timeless(\res{a}{\gamma}{\cidx}),\ \timeless(\okd(a))
\end{gather*}

\subsubsection{Step-indexed equality}

We introduced the following connective for the step-indexed equality.
\begin{gather*}
  a \eqd b \ \defeq\
    \lam{\_}\ \lam{n}\
    a \Eq{n} b
    \quad \note{a, b \col T \ \text{for some OFE \(T\)}}
\end{gather*}
The following properties hold.
\begin{gather*}
  \persistent(a \eqd b) \qqquad
  \text{\(a\) and \(b\) come from a discrete OFE} \imp
    a \eqd b \biyields a = b \br[.1em]
  \yields a \eqd a \qqquad
  a \eqd b \yields b \eqd a \qqquad
  a \eqd b \,\sep\, b \eqd c \yields a \eqd c \br[.0em]
  \text{\(f\) is non-expansive} \imp
    a \eqd b \yields f(a) \eqd f(b)  \br[.0em]
  \Own(a) \,\sep\,
  a \eqd b
    \yields \Own(b) \qquad
  \okd(a) \,\sep\,
  a \eqd b \yields
    \okd(b)
\end{gather*}
