\subsection{Prophecy Resolution}\label{index:pry-res}

We introduce the following relation \(\Dep(v, X) \col \Prop\) over a \(\pi\)-parametrized value \(v \col \PryAsn \to T\) (for some \(T \col \Type\)) and a set of prophecy variables \(X \col \pow \PryVar\).
\begin{equation*}
  \Dep(v, X) \,\defeq\,
    \all{\pi, \pi' \?\st
      (\all{x \in X}\, \pi(x) = \pi'(x))}\
      v(\pi) = v(\pi')
\end{equation*}
It means that \(v\) depends only on the values for the prophecy variables in \(X\).
We have the following lemmas.
\begin{gather*}
  \Dep\bigl(\,
    \lam{\pi} \pi(x), \{x\}
  \,\bigr)
  \newrule{Dep-One} \br[.1em]
  X \sub Y \cand \Dep(v, X) \imp
    \Dep(v, Y)
  \newrule{Dep-Mono} \br[.1em]
  \begin{aligned}
  &
    \bigl(\,
      \all{i \!<\! n} \Dep(v_i, X)
    \,\bigr) \,\imp\,
      \Dep\bigl(\,
        \lam{\pi} f\bigl(v_0(\pi), \ldots, v_{n-1}(\pi)\bigr),\, X
      \,\bigr) \\[-.4em]
  & \hspace{18em}
    \note{f \ \text{does not depend on}\ \pi}
  \end{aligned}
  \newrule{Dep-Compose} \br[-.2em]
  \Dep\bigl(\,
    \lam{\pi} f\bigl(v_0(\pi), \ldots, v_{n-1}(\pi)\bigr),\, X
  \,\bigr) \,\imp\,
    \all{i \!<\! n} \Dep(v_i, X)
  \quad \note{f \ \text{is injective}}
  \newrule{Dep-Decompose}
\end{gather*}

A prophecy resolution item is a record of the type
\begin{equation*}
  \PryResItem \,\defeq\, \bigl(\,
    \pvar \col \PryVar,\
    \val \col \PryAsn \!\to\! \PryTy(x.\tidn)
  \,\bigr),
\end{equation*}
consisting of
(\(\pvar\)) the prophecy variable and
(\(\val\)) the \(\pi\)-parametrized value assigned to the prophecy variable.

A prophecy resolution \(\eP\) is a list of prophecy resolution items, of the type
\begin{equation*}
  \PryRes \,\defeq\,
    \List\, \PryResItem.
\end{equation*}
When we add a new item to a prophecy resolution, we add it to the head of the list;
thus the list of a prophecy resolution goes from the newest to the oldest.

The constraint on a prophecy assignment \(\flr{\eP} \col \PryAsn \to \Prop\)
denoted by a prophecy resolution \(\eP \col \PryRes\)
is defined as follows.
\begin{equation*}
  \flr{(x, v) \cons \eP} (\pi) \,\defeq\,
    \pi(x) = v(\pi) \cand \flr{\eP} (\pi) \qqquad
  \flr{\nil} (\pi) \,\defeq\, \top
\end{equation*}

We define \(\pvars(\eP) \col \powfin \PryVar\) for \(\eP \col \PryRes\)
as the finite set of prophecy variables resolved by \(\eP\),
i.e. as follows.
\begin{equation*}
  \pvars\bigl( (x, \_) \cons \eP \bigr) \,\defeq\,
    \{x\} \,\cup\, \pvars(\eP) \qqquad
  \pvars(\?\nil) \,\defeq\,
    \emp
\end{equation*}

The validity \(\ok(\eP) \col \Prop\) on a prophecy resolution \(\eP \col \PryRes\) is defined as follows.
\begin{equation*}
  \ok\bigl( (x, v) \cons \eP \bigr) \,\defeq\,
    x \notin \pvars(\eP) \cand
    \Dep\bigl(\+
      v,\, \set{y}{y \notin \pvars(\eP)}
    \+\bigr) \cand
    \ok(\eP) \qqquad
  \ok(\?\nil) \,\defeq\, \top
\end{equation*}
It means that we cannot resolve a prophecy variable twice
and the value assigned to a prophecy variable should not depend on the prophecy variables that has been resolved.

The constraint of a prophecy resolution is always satisfiable.
\begin{lemma}[Satisfiability of a Valid Prophecy Resolution]\label{lemma:Sat-PryRes}
For any \(\eP_* \col \PryRes\) satisfying \(\ok(\eP_*)\),
there exists some \(\pi_* \col \PryAsn\) satisfying \(\flr{\eP_*} (\pi_*)\).
\end{lemma}
\begin{proof}
We introduce the following function \(\modify_\eP (\pi) \col \PryAsn\) over \(\eP \col \PryRes\) and \(\pi \col \PryAsn\).
\begin{equation*}
  \modify_{(x, v) \cons \eP} (\pi) \defeq
    \modify_\eP (\mapupd{\pi}{x}{v(\pi)}) \qqquad
  \modify_\nil (\pi) \defeq \pi
\end{equation*}
We can inductively show the following for any \(\eP\) and \(\pi\).
\begin{equation*}
  \all{y \notin \pvars(\eP)}\,
    \pi(y) = \modify_\eP (\pi)(y).
\end{equation*}
Using this,
we can inductively show
\(\flr{\eP}(\modify_\eP (\pi))\)
for any \(\pi\) and \(\eP\) satisfying \(\ok(\eP)\).

Now we can set \(\pi_* \!\defeq\! \modify_{\eP_*}\! (\pi_\any)\).
\end{proof}
