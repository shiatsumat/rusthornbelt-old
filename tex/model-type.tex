\subsection{Semantic Types}\label{index:model-type}

\subsubsection{Semantic Type}

A semantic type \(\tau \col \SemTy\) is a record of the type
\begin{align*}
&\bigl(\,
    \type \col \Type,\quad
    \lft \col \Lft,\quad
    \size \col \NN,\quad
    \scan \col \Cval, \br[-.3em]
& \hspace{1em}
    \own \col\,
      \ThId \X \Cval^\size \X (\PryAsn \!\to\! \type)
      \to \IProp, \br[-.3em]
& \hspace{1em}
    \shr \col\,
      \Lft \X \ThId \X \Cval^\size \X (\PryAsn \!\to\! \type)
      \to \IProp
  \,\bigr),
\end{align*}
consisting of
(\(\type\))
the pure type representing data of the type,
(\(\lft\))
the lifetime of the type,
(\(\size\))
the number of memory cells required at the shallowest level for the type,
(\(\scan\))
the auxiliary function for scanning the whole data to mitigate later modalities,
(\(\own\))
the resource for owning the data with respect to
a thread id,
cell values and
a \(\pi\)-parametrized pure value,
(\(\shr\))
the resource for sharing the data with respect to
a lifetime,
a thread id,
cell values and
a \(\pi\)-parametrized pure value,
for which we introduce the following notations
\begin{equation*}
  \xws \is{\xt} \tau \rfn{v} \,\defeq\,
    \tau.\own(\xt, \xws, v) \qqquad
  \xws \isshr{\alpha}{\xt} \tau \rfn{v} \,\defeq\,
    \tau.\shr(\alpha, \xt, \xws, v)
\end{equation*}
and which we require to satisfy the following rules.
\begin{gather*}
  \ex{\tid}\ \tau.\type = \PryTy(\?\tid)
  \newrule{Ty-PryTy} \br[.0em]
  \begin{aligned}
  &
    \Ctx_\lft
    \!\!\yields\
    \hoare{
      \xws \is{\xt} \tau \rfn{v} \,\sep\,
      [\tau.\lft]_q
    }{ \scan(\xws) }{
      \ex{X \st \Dep(v, X)} \\[-.3em]
  & \hspace{5em}
      \ex{q'}\,
      [X]_{q'} \,\sep\,
      \bigl(\+
        [X]_{q'}
        \wandupdf{\iN_\lft}
        \xws \is{\xt} \tau \rfn{v} \sep
        [\tau.\lft]_q
      \+\bigr)
    }{ \iN_\lft }
  \end{aligned}
  \newrule{Ty-Own-Pry} \br[.0em]
  \begin{aligned}
  &
    \Ctx_\lft
    \!\!\yields\
    \hoare{
      \xws \isshr{\alpha}{\xt} \tau \rfn{v} \,\sep\,
      [\tau.\lft]_q
    }{ \scan(\xws) }{
      \ex{X \st \Dep(v, X)} \\[-.4em]
  & \hspace{5em}
      \ex{q'}
      [X]_{q'} \,\sep\,
      \bigl(\+
        [X]_{q'}
        \wandupdf{\iN_\lft}
        \xws \isshr{\alpha}{\xt} \tau \rfn{v} \sep
        [\tau.\lft]_q
      \+\bigr)
    }{ \iN_\lft }
  \end{aligned}
  \newrule{Ty-Shr-Pry} \br[.0em]
  \persistent\bigl( \xws \isshr{\alpha}{\xt} \tau \rfn{v} \bigr)
  \newrule{Ty-Shr-Pers} \br[.0em]
  \beta \tle \alpha \,\sep\,
  \xws \isshr{\alpha}{\xt} \tau \rfn{v}
  \,\yields\,
  \xws \isshr{\beta}{\xt} \tau \rfn{v}
  \newrule{Ty-Shr-Mono-Lft} \br[.0em]
  \Ctx_\lft
  \!\!\yields\
  \hoare{
    \reffull{\alpha} \bigl(\+
      \xws \is{\xt} \tau \rfn{v}
    \+\bigr) \,\sep\,
    [\alpha \?\tand\? \tau.\lft]_q
  }{ \scan(\xws) }{
    \xws \isshr{\alpha}{\xt} \tau \rfn{v} \,\sep\,
    [\alpha \?\tand\? \tau.\lft]_q
  }{ \iN_\lft }
  \newrule{Ty-Own-Shr}
\end{gather*}
The rule \cref{rule:Ty-PryTy} says that
the pure type of \(\tau\) should correspond to some prophecy type id;
\(\PryTyId\) and \(\PryTy\) should be constructed so that it accommodates every pure type for the semantic types we want
(we omit the construction of \(\PryTyId\) and \(\PryTy\) in this paper).
The rule \cref{rule:Ty-Own-Pry} says that,
with the help of the lifetime token and the function \(\scan\),
a data-owning resource \(\xws \is{\xt} \tau \rfn{v}\)
can be divided into
(i) a partial token \([X]_{q'}\) on the prophecy variables that the value \(v\) depends on and
(ii) a view shift that returns a data-owning resource \(\xws \is{\xt} \tau \rfn{v}\) by inputting \([X]_q'\).
The rule \cref{rule:Ty-Shr-Pry} is similar to \cref{rule:Ty-Own-Pry},
but is for a data-sharing resource instead of a data-owning resource.
The rules \cref{rule:Ty-Shr-Pers} and \cref{rule:Ty-Shr-Mono-Lft} say that
the data-sharing resource should be persistent and anti-monotone over the lifetime.
The rule \cref{rule:Ty-Own-Shr} says that,
we can transform a full reference to a data-owning resource into a data-sharing resource,
with the help of the lifetime token \([\alpha]_q\) and the function \(\scan\).
For \cref{rule:Ty-Own-Pry}, \cref{rule:Ty-Shr-Pry} and \cref{rule:Ty-Own-Shr},
we use \(\scan\) to mitigate later modalities.\footnote{\
  If we work on transfinite Iris instead of Iris,
  we can use the `super fancy later modality' \((\updf{\iE}\later)^\omega\, P\) for each physical step,
  and we can naturally avoid the use of \(\scan\).
}

We also use the following shorthand.
\begin{gather*}
  \xw \is{\xt} \tau \rfn{v} \,\defeq\,
    [\xw] \is{\xt} \tau \rfn{v} \qqquad
  \abs{\tau} \,\defeq\, \tau.\size \br[.0em]
  \scanat_\tau \,\defeq\,
    \fn(a) \setto \tau.\scan\bigl(\+
      *(a \!+\! 0),\, *(a \!+\! 1),\, \ldots,\, *(a \!+\! (\abs{\tau} \!-\! 1))
    \+\bigr)
\end{gather*}
The following lemma holds for \(\scanat_\tau\).
\begin{equation*}
  \len \xws = \abs{\tau} \,\imp\,
    \hoare{
      P
    }{ \tau.\scan(\xws) }{
      Q
    }{ \iE }
    \,\yields\,
    \hoare{
      \xl \mto[q] \xws \,\sep\,
      \later P
    }{ \scanat_\tau(\xl)} {
      \xl \mto[q] \xws \,\sep\, Q
    }{ \iE }
\end{equation*}

\subsubsection{Simple semantic types}

The integer type \(\Int\) is modeled as follows.
\begin{align*}
&
  \Int \,\defeq\, \bigl(\,
    \type \col \ZZ,\quad
    \lft \col \eps,\quad
    \size \col 1,\quad
    \scan \col\,
      \fn(\_) \setto \no, \br[-.3em]
& \hspace{2em}
    \own \col\,
      \lam{\_, \xws, v}\,
      \ex{n \st \xws = [n]}\,
      \all{\?\pi} v_\pi = n,\quad
    \shr \col\,
      \lam{\_, \_, \xws, v}\,
      \ex{n \st \xws = [n]}\,
      \all{\?\pi} v_\pi = n
  \,\bigr)
\end{align*}

The size-\(n\) uninitialized-data type \(\no_n\) is modeled as follows.
\begin{align*}
&
  \no_n \,\defeq\, \bigl(\,
    \type \col \Unit,\quad
    \lft \col \eps,\quad
    \size \col n,\quad
    \scan \col\,
      \fn(\_, \ldots, \_) \setto \no, \br[-.3em]
& \hspace{4em}
    \own \col\,
      \lam{\_, \_, \_} \top,\quad
    \shr \col\,
      \lam{\_, \_, \_, \_} \top
  \,\bigr)
\end{align*}

The pair type \(\tau_0 \X \tau_1\) and the sum type \(\tau_0 + \tau_1\) are modeled as follows.
\begin{align*}
&
  \tau_0 \X \tau_1 \,\defeq\, \bigl(\,
    \type \col
      \tau_0.\type \X \tau_1.\type,\quad
    \lft \col
      \tau_0.\lft \tand \tau_1.\lft,\quad
    \size \col
      \abs{\tau_0} + \abs{\tau_1}, \br[-.3em]
& \hspace{2em}
    \scan \col\,
      \fn(\seq{a}^{\abs{\tau_0}}, \seq{b}^{\abs{\tau_1}}) \setto
        \tau_0.\scan(\seq{a});\+
        \tau_1.\scan(\seq{b}), \br[-.1em]
& \hspace{2em}
    \own \col\,
      \lam{\xt, \xws, v}\,
      \xws[..\abs{\tau_0}] \is{\xt} \tau_0
        \rfn{\lam{\pi} v_\pi.0} \,\sep\,
      \xws[\abs{\tau_0}..] \is{\xt} \tau_1
        \rfn{\lam{\pi} v_\pi.1}, \br[-.3em]
& \hspace{2em}
    \shr \col\,
      \lam{\alpha, \xt, \xws, v}\,
      \xws[..\abs{\tau_0}] \isshr{\alpha}{\xt} \tau_0
        \rfn{\lam{\pi} v_\pi.0} \,\sep\,
      \xws[\abs{\tau_0}..] \isshr{\alpha}{\xt} \tau_1
        \rfn{\lam{\pi} v_\pi.1}
  \,\bigr) \br[.3em]
&
  \tau_0 + \tau_1 \,\defeq\, \bigl(\,
    \type \col
      \tau_0.\type + \tau_1.\type,\quad
    \lft \col
      \tau_0.\lft \tand \tau_1.\lft,\quad
    \size \col
      1 + \max \,\{\abs{\tau_0},\+ \abs{\tau_1}\}, \br[-.1em]
& \hspace{2em}
    \scan \col\,
      \fn(i, \seq{a}) \setto
        \case i \of [\+
          \tau_0.\scan(\seq{a}[..\abs{\tau_0}]),\
          \tau_1.\scan(\seq{a}[..\abs{\tau_1}])
        \+], \br[-.1em]
& \hspace{2em}
    \own \col\,
      \lam{\xt, \xws, v}\,
      \ex{i, v' \st
        \xws[0] \!=\! i \land
        \all{\?\pi} v_\pi \!=\! \inj_i (v'_\pi)}\
      \xws[1 .. 1 \!+\! \abs{\tau_i}] \is{\xt}
        \tau_i \rfn{v'} \br[-.3em]
& \hspace{2em}
    \shr \col\,
      \lam{\alpha, \xt, \xws, v}\,
      \ex{i, v' \st
        \xws[0] \!=\! i \land
        \all{\?\pi} v_\pi \!=\! \inj_i (v'_\pi)}\
      \xws[1 .. 1 \!+\! \abs{\tau_i}] \isshr{\alpha}{\xt}
        \tau_i \rfn{v'}
  \,\bigr)
\end{align*}

As a demonstration, we write down here the proof of \cref{rule:Ty-Own-Pry} on \(\tau_0 \X \tau_1\).

\begin{proof}[Proof of \cref{rule:Ty-Own-Pry} on \(\tau_0 \X \tau_1\)]
  Suppose we have the following, where \(\len \xws_i = \abs{\tau_i}\).
  \begin{equation*}
    \xws_0 \app \xws_1 \is{\xt} \tau_0 \X \tau_1
      \rfn{\lam{\pi} (v_0(\pi), v_1(\pi))}
  \end{equation*}
  It can be split into
  \(\xws_0 \is{\xt} \tau_0 \rfn{v_0}\) and
  \(\xws_1 \is{\xt} \tau_1 \rfn{v_1}\).
  Suppose also we have the lifetime token \([(\tau_0 \X \tau_1).\lft]_q\),
  which can be split into \([\tau_0.\lft]_q\) and \([\tau_1.\lft]_q\).

  After the scan \((\tau_0 \!\X\! \tau_1).\scan(\xws_0 \!\app\! \xws_1)\),
  which performs \(\tau_0.\scan(\xws_i)\) for each \(i\),
  for some \(X_0, X_1\)
  satisfying \(\Dep(v_i, X_i)\) for each \(i\),
  and for some \(q'_0, q'_1\),
  we get the partial prophecy tokens \([X_0]_{q'_0}, [X_1]_{q'_1}\)
  and the view shifts
  that can be merged in parallel into the following view shift.
  \begin{equation*}
    [X_0]_{q'_0} \sep
    [X_1]_{q'_1}
    \,\wandupdf{\iN_\lft}\,
    \xws_0 \app \xws_1 \is{\xt} \tau_0 \X \tau_1
      \rfn{\lam{\pi} (v_0(\pi), v_1(\pi))} \sep
    [(\tau_0 \X \tau_1).\lft]_q
\end{equation*}
  Let us set \(X \!\defeq\! X_0 \cup X_1\)
  and \(q' \!\defeq\! \min \{ q'_0, q'_1 \}\).
  Now \(\Dep(\lam{\pi} (v_0(\pi), v_1(\pi)),\, X)\) holds,
  because \(\Dep(v_i, X_i)\) holds for each \(i\).
  Also, we can perform the following separation.
  \begin{equation*}
    [X_0]_{q'_0} \,\sep\,
    [X_1]_{q'_1}
      \ \yields\
      [X]_{q'} \,\sep\,
      (\+
        [X]_{q'} \+\wand\+
        [X_0]_{q'_0} \sep [X_1]_{q'_1}
      \+)
  \end{equation*}
  Therefore, we get the expected postcondition of \cref{rule:Ty-Own-Pry}.
\end{proof}

The ownership pointer type \(\rown_n \tau\) is modeled as follows,
where the number \(n \col \NN\) denotes the size of the allocation block the data belongs to.
\begin{align*}
&
  \rown_n \tau \,\defeq\, \bigl(\,
    \type \col \tau.\type,\quad
    \lft \col \tau.\lft,\quad
    \size \col 1,\quad
    \scan \col \scanat_\tau, \br[-.2em]
& \hspace{2em}
    \own \col\,
      \lam{\xt, \xws, v}\,
      \ex{\xl \st \xws \!=\! [\xl]}\,
      \Dealloc(\xl, \abs{\tau}, n) \,\sep\,
      \ex{\xws'}\,
      \xl \mto \xws' \,\sep\,
      \later\,
        \xws' \is{\xt} \tau \rfn{v}, \br[-.4em]
& \hspace{2em}
    \shr \col\,
      \lam{\alpha, \xt, \xws, v}\,
      \ex{\xl \st \xws \!=\! [\xl]}\,
      \ex{\xws'}\,
        \reffrac{\alpha} (\lam{q} \xl \mto[q] \xws') \,\sep\,
        \later\, \xws' \isshr{\alpha}{\xt} \tau \rfn{v}
  \,\bigr)
\end{align*}
Because of the later modality used in the data-owning/sharing resources,
\((\own_n \tau).\own/\shr\) are contractive over \(\tau.\own/\shr\).

The vector type is modeled as follows.
\begin{align*}
&
  \rvec \tau \,\defeq\, \bigl(\,
    \type \col \List (\tau.\type),\quad
    \lft \col \tau.\lft,\quad
    \size \col 3, \br[.0em]
& \hspace{2em}
    \scan \col\, \fn(a, \_, b) \setto
      \For i \by 0..b \Do
        \scanat_\tau(a + i \X \abs{\tau}), \br[.0em]
& \hspace{2em}
    \own \col\,
      \lam{\xt, \xws, v}\,
      \ex{\xl, m, n \st
        \xws \!=\! [\xl, m, n] \land
        m \?\ge\? n \land
        \all{\?\pi}\? \len v_\pi = n} \br[-.3em]
& \hspace{4em}
      \Dealloc(\xl,\, m \!\cdot\! \abs{\tau},\, m \!\cdot\! \abs{\tau}) \,\sep\,
      \ex{\xws' \st
        \len \xws' = m \!\cdot\! \abs{\tau}}\
      \xl \mto \xws' \,\sep {} \br[-.3em]
& \hspace{4em} \slim
      \bigsep_{i < n}\,
        \later\,
        \xws'[i \!\cdot\! \abs{\tau} \,..\, (i \!+\! 1) \!\cdot\! \abs{\tau}]
          \is{\xt} \tau \rfn{\lam{\pi} v_\pi[i]} \br[.1em]
& \hspace{2em}
    \shr \col\,
      \lam{\alpha, \xt, \xws, v}\,
      \ex{\xl, m, n \st
        \xws \!=\! [\xl, m, n] \land
        m \?\ge\? n \land
        \all{\?\pi}\? \len v_\pi = n} \br[-.3em]
& \hspace{4em} \slim
      \ex{\xws' \st
        \len \xws' = m \!\cdot\! \abs{\tau}}\,
      \reffrac{\alpha} (\lam{q}\, \xl \mto[q] \xws') \,\sep {} \br[-.4em]
& \hspace{4em} \slim
      \bigsep_{i < n}\,
        \later\,
        \xws'[i \!\cdot\! \abs{\tau} \,..\, (i \!+\! 1) \!\cdot\! \abs{\tau}]
          \isshr{\alpha}{\xt} \tau \rfn{\lam{\pi} v_\pi[i]}
  \,\bigr)
\end{align*}
The data \([\xl, m, n]\) of a vector consists of
the location of the allocated block \(\xl\), the capacity of the block \(m\), and the length of the vector \(n\).
Since the vector type \(\rvec \tau\) is a kind of pointer type, like \(\own_n \tau\),
we use the later modality in the data-owning/sharing resources.

\subsubsection{Shared reference type}

The shared reference type \(\refshr{\alpha} \tau\) is modeled as follows,
using the data-sharing function of \(\tau\).
\begin{align*}
&
  \refshr{\alpha} \tau \,\defeq\, \bigl(\,
    \type \col \tau.\type,\quad
    \lft \col \alpha \tand \tau.\lft,\quad
    \size \col 1,\quad
    \scan \col \scanat_\tau, \br[-.3em]
& \hspace{2em}
    \own \col\,
      \lam{\xt, \xws, v}\,
      \ex{\xl \st \xws \!=\! [\xl]}\,
      \ex{\xws'}\,
      \reffrac{\alpha} (\lam{q}\, \xl \mto[q] \xws') \,\sep\,
      \later\, \xws' \isshr{\alpha}{\xt} \tau \rfn{v}, \br[-.3em]
& \hspace{2em}
    \shr \col\,
      \lam{\_, \xt, \xws, v}\,
      \ex{\xl \st \xws \!=\! [\xl]}\,
      \ex{\xws'}\,
        \reffrac{\alpha} (\lam{q}\, \xl \mto[q] \xws') \,\sep\,
        \later\, \xws' \isshr{\alpha}{\xt} \tau \rfn{v}
  \,\bigr)
\end{align*}
Interestingly, the data-sharing function \((\refshr{\alpha} \tau).\shr\) does not depend on its lifetime argument,
because we can take out the persistent resources
from the full reference given in the rule \cref{rule:Ty-Own-Shr}.
We use the later modality in \(\refshr{\alpha} \tau.\own/\shr\) to make them contractive over \(\tau.\shr\).

\begin{proof}[Proof of \cref{rule:Ty-Own-Pry} on \(\refshr{\alpha} \tau\)]
  By \cref{rule:Ty-Shr-Pry} on \(\tau\).
\end{proof}

\subsubsection{Mutable reference type}

We introduce the following RA for the mutable reference type, which we register to the global camera.\footnote{\
  Here, as in \(\PRY\),
  in place of \(\PryVar \tofine -\)
  we can use \(\PryTyId \to \NN \tofine -\),
  which can be easier to implement.
}
\begin{equation*} \slim
  \MUT \,\defeq\,
    (x \col \PryVar) \ \tofine\
      \AUTH \bigl(\+ \EX\+ \bigl(\+ \Maybe\+ \bigl(
        \PryAsn \?\to\? \PryTy(x.\tidn)
      \+\bigr) \+\bigr) \+\bigr)
\end{equation*}
In this RA,
for a finite number of prophecy variables \(x\),
we can manage a \(\pi\)-parametrized value.
We use some fixed ghost name \(\gamma_\MUT\) for \(\MUT\).

We introduce the Iris propositions named
a mutation observer \(\MutObs_x(v)\)
and a prophecy control \(\PryCtrl(x, v)\)
(for \(x \col \PryVar\) and \(v \col \PryAsn \!\to\! \PryTy(x.\tidn)\))
defined as follows.
\begin{gather*}
  \MutObs_x(v) \,\defeq\,
    \res{\mapone{x}{\repl (\exn v)}}{\gamma_\MUT}{\MUT} \br[.3em]
  \PryCtrl(x, v) \,\defeq\,
    \bigl(\,
      \res{\mapone{x}{\prim (\exn v)}}{\gamma_\MUT}{\MUT} \,\sep\,
      [x]_1
    \+\bigr) \,\lor\,
    \bigl(\,
      \res{\mapone{x}{\prim (\exn \no)}}{\gamma_\MUT}{\MUT} \,\sep\,
      \Resolver(\lam{\pi}\? \pi(x),\, v)
    \+\bigr)
\end{gather*}
A mutation observer \(\MutObs_x(v)\) observes with half ownership that the \(\pi\)-parametrized value assigned to \(x\) by \(\MUT\) is \(v\).
A prophecy control \(\PryCtrl(x, v)\) either
(i) observes with half ownership that the value assigned to \(x\) by \(\MUT\) is \(v\)
and has the full prophecy token \([x]_1\), or
(ii) observes with half ownership that no value is assigned to \(x\) by \(\MUT\)
and has a resolver \(\Resolver(x, v)\).
We have the following lemmas.
\begin{gather*}
  \timeless\bigl( \MutObs_x(v) \bigr) \br[.1em]
  \yields\!\! \upd\! \ex{x \st x.\tidn = \tid}\,\
    \MutObs_x(v) \,\sep\,
    \PryCtrl(x, v)
  \newrule{MutObs-PryCtrl-Intro} \br[.1em]
  \MutObs_x(v) \,\sep\,
  \PryCtrl(x, v')
    \ \yields\
    v = v'
  \newrule{MutObs-PryCtrl-Agree} \br[.1em]
  \begin{aligned}
  &
    \MutObs_x(v) \,\sep\,
    \PryCtrl(x, v)
      \ \yields {} \\[-.4em]
  & \hspace{1em}
      \MutObs_x(v) \,\sep\,
      [x]_1 \,\sep\,
      \bigl(\+ [x]_1 \,\wand\, \PryCtrl(x, v) \+\bigr)
  \end{aligned}
  \newrule{MutObs-PryCtrl-Token} \br[.1em]
  \MutObs_x(v) \,\sep\,
  \PryCtrl(x, v)
    \ \yields\!\! \upd\
    \MutObs_x(v') \,\sep\,
    \PryCtrl(x, v')
  \newrule{MutObs-PryCtrl-Update} \br[.1em]
  \begin{aligned}
  &
    \Dep(v, Y) \ \imp\
      \MutObs_x(v) \,\sep\,
      \PryCtrl(x, v) \,\sep\,
      [Y]_q \\[-.3em]
  & \hspace{1em}
        \sep\! \Ctx_\pry
        \!\!\yields\!\!
          \updf{\iN_\pry}\,\
        \know{\lam{\pi} \pi(x) = v_\pi} \,\sep\,
        \PryCtrl(x, v) \,\sep\,
        [Y]_q
  \end{aligned}
  \newrule{MutObs-PryCtrl-Resolve} \br[.1em]
  \begin{aligned}
  &
    \Dep(w, Y) \ \imp\
      \MutObs_x(v) \,\sep\,
      \PryCtrl(x, v) \,\sep\,
      [Y]_q \\[-.3em]
  & \hspace{1em}
        \sep\! \Ctx_\pry
        \!\!\yields\!\!
          \updf{\iN_\pry}\,\
        \know{\lam{\pi} \pi(x) = w_\pi} \,\sep\,
        [Y]_q \,\sep\,
        \bigl(\,
          \all{v'}\,
          \Resolver(w, v') \,\wand\,
          \PryCtrl(x, v')
        \,\bigr)
  \end{aligned}
  \newrule{MutObs-PryCtrl-PreResolve} \br[-.1em]
  \PryCtrl(x, v)
    \ \,\sep\! \Ctx_\pry
    \!\!\yields\,\
    \Resolver(\lam{\pi}\? \pi(x),\, v)
  \newrule{PryCtrl-Resolver}
\end{gather*}
The lemma \cref{rule:MutObs-PryCtrl-Intro} says that
we can pick out a fresh prophecy variable \(x\) of a specified type id
and, for an arbitrary value \(v \col \PryAsn \!\to\! \PryTy(x.\tidn)\),
get a mutation observer \(\MutObs_x(v)\) and a prophecy control \(\PryCtrl(x, v)\).
When we have a mutation observer and a prophecy control,
we know that their \(\pi\)-parametrized values agree (\cref{rule:MutObs-PryCtrl-Agree}),
we can split out a full prophecy token \([x]_1\) from the prophecy control (\cref{rule:MutObs-PryCtrl-Token}),
and we can simultaneously update the \(\pi\)-parametrized values of the two (\cref{rule:MutObs-PryCtrl-Update}).
The lemma \cref{rule:MutObs-PryCtrl-Resolve} says that,
by consuming a mutation observer \(\MutObs_x(v)\),
with the help of a prophecy control \(\PryCtrl(x, v)\) and a relevant partial prophecy token,
we can get a prophecy assertion \(\know{\lam{\pi} \pi(x) = v_\pi}\).
The lemma \cref{rule:MutObs-PryCtrl-PreResolve} is an advanced version of the previous lemma:
by consuming a mutation observer and a prophecy control,
we can first get \(\know{\lam{\pi} \pi(x) = w_\pi}\) with the help of a relevant partial prophecy token,
and then get a new prophecy control \(\PryCtrl(x, v')\) for any value \(v'\)
out of a resolver \(\Resolver(w, v')\).
The lemma \cref{rule:PryCtrl-Resolver} says that
we can get a resolver \(\Resolver(\lam{\pi}\? \pi(x), v)\)
out of a prophecy control \(\PryCtrl(x, v)\).

\begin{proof}[Proof of \cref{rule:MutObs-PryCtrl-Intro}]
  We simultaneously check the global resources we have at the RAs \(\PRY\) (of \(\gamma_\PRY\)) and \(\MUT\) (of \(\gamma_\MUT\)).
  There are only a finite number of prophecy variables occupied by the global resources of \(\PRY\) and \(\MUT\)
  (i.e. appearing in the prophecy resolution of \(\PRY\), the support of the fractional map of \(\PRY\), or the support of the map of \(\MUT\)).
  So we can pick out a fresh prophecy variable \(x\) (of a specified type id)
  not occupied by \(\PRY\) or \(\MUT\).
  After frame-preserving update,
  we obtain \([x]_1\) and
  \(\res{\mapone{x}{
    \prim (\exn v) \cdot \repl (\exn v)
  }}{\gamma_\MUT}{\MUT}\).
\end{proof}

\begin{proof}[Proof of \cref{rule:MutObs-PryCtrl-Agree} and \cref{rule:MutObs-PryCtrl-Token}]
  When we have a mutation observer and a prophecy control,
  by agreement on the RA \(\AUTH(\EX({\cdots}))\),
  we know that the disjunction of the prophecy control takes the left-hand side, which has a full prophecy token,
  and that the \(\pi\)-parametrized values of the mutation observer and the prophecy control agree.
\end{proof}

\begin{proof}[Proof of \cref{rule:MutObs-PryCtrl-Update}]
  We take out \(\res{
    \mapone{x}{\prim (\exn v) \cdot \repl (\exn v)}
  }{\gamma_\MUT}{\MUT}\) from the mutation observer and the prophecy control,
  and update it into \(\res{
    \mapone{x}{\prim (\exn v') \cdot \repl (\exn v')}
  }{\gamma_\MUT}{\MUT}\).
\end{proof}

\begin{proof}[Proof of \cref{rule:MutObs-PryCtrl-Resolve}]
  Out of \(\MutObs_x(v)\) and \(\PryCtrl(x, v)\),
  we get \([x]_1\) and
  \(P \defeq \res{\mapone{x}{
    \prim (\exn v) \cdot \repl (\exn v)
  }}{\gamma_\MUT}{\MUT}\).
  By consuming \([x]_1\),
  with the help of a relevant partial prophecy token,
  we get a prophecy assertion \(\know{\lam{\pi} \pi(x) = v_\pi}\)
  by \cref{rule:PryVar-Resolve}.
  By consuming \(P\), after update,
  we get \(Q \!\defeq\! \res{\mapone{x}{
    \prim (\exn \no)
  }}{\gamma_\MUT}{\MUT}\).
  We get a resolver \(\Resolver(x, v)\) from \(\know{\lam{\pi} \pi(x) = v_\pi}\) by \cref{rule:PryAstn-Resolver}.
  From \(Q\) and \(\Resolver(x, v)\),
  we can construct (the right-hand side of the disjunction of) a prophecy control \(\PryCtrl(x, v)\).
\end{proof}

\begin{proof}[Proof of \cref{rule:MutObs-PryCtrl-PreResolve}]
  Like \cref{rule:MutObs-PryCtrl-Resolve},
  by consuming a mutation observer and a prophecy control,
  we get a prophecy assertion \(\know{\lam{\pi} \pi(x) = w_\pi}\)
  and an observation \(Q \!\defeq\! \res{\mapone{x}{
    \prim (\exn \no)
  }}{\gamma_\MUT}{\MUT}\).
  Now we can turn \(Q\) into the expected magic wand that inputs a resolver and outputs a prophecy control,
  using \cref{rule:Resolver-Modify} and \(\know{\lam{\pi} \pi(x) = w_\pi}\).
\end{proof}

\begin{proof}[Proof of \cref{rule:PryCtrl-Resolver}]
  When the disjunction of the prophecy control takes the right-hand side,
  we can just use the resolver of it.
  When the disjunction takes the left-hand side, we use \cref{rule:PryToken-Resolver} to turn \([x]_1\) into a resolver.\footnote{\
    Note that this case occurs when a mutable reference has been leaked without resolving its prophecy variable.
  }
\end{proof}

Also, we introduce the following record type for modeling a mutable reference,
whose element consists of the current value (\(\cur\)) and the future value at the end of the borrow (\(\fut\)).
\begin{equation*}
  \Mut T \defeq \bigl(\, \cur \col T,\ \fut \col T \,\bigr)
\end{equation*}

Now we model the mutable reference type \(\refmut{\alpha} \tau\) as follows.
\begin{align*}
&
  \refmut{\alpha} \tau \,\defeq\, \bigl(\,
    \type \col \Mut (\tau.\type),\quad
    \lft \col \alpha \tand \tau.\lft,\quad
    \size \col 1,\quad
    \scan \col \scanat_\tau, \br[.0em]
& \hspace{1em}
    \own \col\,
      \lam{\xt, \xws, v}\,
      \ex{\xl \st \xws = [\xl]}\,
      \ex{x \st
        \all{\?\pi} v_\pi.\fut \?=\? \pi(x)} \br[-.3em]
& \hspace{2em}
      \MutObs_x(\lam{\pi} v_\pi.\cur) \,\sep\,
      \reffull{\alpha}\, \bigl(\,
        \ex{\xws', v'}\,
        \xl \mto \xws' \,\sep\,
        \xws' \is{\xt} \tau \rfn{v'} \,\sep\,
        \PryCtrl(x, v')
      \,\bigr) \br[.1em]
& \hspace{1em}
    \shr \col\,
      \lam{\beta, \xt, \xws, v}\,
      \ex{\xl \st \xws = [\xl]}\,
      \ex{\xws'}\,
      \ex{x \st
        \all{\?\pi} v_\pi.\fut \?=\? \pi(x)} \br[-.4em]
& \hspace{2em}
      \reffrac{\beta \tand \alpha} (\lam{q} [x]_q) \,\sep\,
      \reffrac{\beta \tand \alpha} (\lam{q} \xl \mto[q] \xws') \,\sep\,
      \xws' \isshr{\beta \tand \alpha}{\xt} \tau
        \rfn{\lam{\pi} v_\pi.\cur}
  \,\bigr)
\end{align*}
The data-owning resource is encoded using the mutation observer and the prophecy control,
whose properties have been discussed above.

\begin{proof}[Proof of \cref{rule:Ty-Own-Pry} on \(\refmut{\alpha} \tau\)]
  Suppose we have the following
  (by definition, the form of a data-owning resource on \(\refmut{\alpha} \tau\) is restricted to the form seen here).
  \begin{equation*}
    \xl \is{\xt} \refmut{\alpha} \tau
      \rfn{\lam{\pi} (v_\pi, \pi(x))} \,\sep\,
    [(\refmut{\alpha} \tau).\lft]_q
  \end{equation*}
  Note that \([(\refmut{\alpha} \tau).\lft]_q \biyields [\alpha]_q \,\sep\, [\tau.\lft]_q\) holds and
  that for any \(X\) satisfying \(\Dep(v, X)\) we have \(\Dep(\lam{\pi} (v_\pi, \pi(x)),\, X \cup \{x\})\).

  By accessing the full reference of the mutable reference by \cref{rule:FullRef-Access},
  using \cref{rule:MutObs-PryCtrl-Agree} and \cref{rule:MutObs-PryCtrl-Token},
  we get the following,
  removing a later modality by the first step of \(\scan\).
  \begin{equation*}
    \xws' \is{\xt} \tau \rfn{v} \,\sep\,
    [x]_1 \,\sep\,
    \bigl(\,
      \xws' \is{\xt} \tau \rfn{v} \,\sep\,
      [x]_1
      \,\wandupdf{\iN_\lft}\,
      \xl \is{\xt} \refmut{\alpha} \tau
        \rfn{\lam{\pi} (v_\pi, \pi(x))} \,\sep\,
      [\alpha]_q
    \,\bigr)
  \end{equation*}
  Combined with the thing we get by using \cref{rule:Ty-Own-Pry} on \(\tau\),
  we get the expected postcondition.
\end{proof}

\begin{proof}[Proof of \cref{rule:Ty-Shr-Pry} on \(\refmut{\alpha} \tau\)]
  It is similar to the proof of \cref{rule:Ty-Own-Pry} on \(\refmut{\alpha} \tau\).
  Just by accessing the fractured reference \(\reffrac{\beta \tand \alpha} (\lam{q} [x]_q)\)
  in a data-sharing resource of \(\refmut{\alpha} \tau\) under \(\beta\),
  we can get a fractional token \([x]_q\).
\end{proof}

\begin{proof}[Proof of \cref{rule:Ty-Own-Shr} on \(\refmut{\alpha} \tau\)]
  Suppose we have a full reference under \(\beta\) to a data-owning resource of \(\refmut{\alpha} \tau\).
  First, we freeze \(\xl, x\) by \cref{rule:FullRef-Freeze},
  then split the outer full reference into two parts by \cref{rule:FullRef-Split},
  and then unnest the second part (of the form \(\reffull{\beta} \reffull{\alpha} \cdots\)) by \cref{rule:FullRef-Unnest}.
  We remove the later modality by the first step of \((\refmut{\alpha} \tau).\scan\).
  Now we have the following.
  \begin{equation*}
    \reffull{\beta} \MutObs_x(\lam{\pi} v_\pi.\cur) \,\sep\,
    \reffull{\beta \tand \alpha} \bigl(\,
      \ex{\xws', v'}\,
      \xl \mto \xws' \,\sep\,
      \xws' \is{\xt} \tau \rfn{v'} \,\sep\,
      \PryCtrl(x, v')
    \,\bigr)
  \end{equation*}
  We freeze \(\xws', v'\) of the second full reference by \cref{rule:FullRef-Freeze},
  and then split the full reference into three full references by \cref{rule:FullRef-Split}.
  We merge the full references to the mutation observer and the prophecy control by \cref{rule:FullRef-Merge}.
  Now we have the following.
  \begin{equation*}
    \reffull{\beta \tand \alpha} \bigl(\+
      \MutObs_x(\lam{\pi} v_\pi.\cur) \+\sep\+
      \PryCtrl(x, v')
    \+\bigr) \,\sep\,
    \reffull{\beta \tand \alpha} \xl \mto \xws' \,\sep\,
    \reffull{\beta \tand \alpha} \xws' \is{\xt} \tau \rfn{v'}
  \end{equation*}
  Accessing the first full reference by \cref{rule:FullRef-Access},
  we know \(v' = \lam{\pi} v_\pi.\cur\)
  by \cref{rule:MutObs-PryCtrl-Agree}.
  Also, we can turn the full reference into \(\reffull{\beta \tand \alpha} [x]_1\) by \cref{rule:FullRef-Modify} and \cref{rule:MutObs-PryCtrl-Token}.
  Now by \cref{rule:FullRef-FracRef},
  we change the first and second full references into fractured references.
  Also, by \cref{rule:Ty-Own-Shr} on \(\tau\),
  we change the third full reference into a data-sharing resource,
  using \(\tau.\scan\) over \(\xws'\).
  Now we get the expected postcondition.
\end{proof}
