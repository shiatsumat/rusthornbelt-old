\subsection{Resource Algebras and Cameras}\label{index:iris-cmra}

\subsubsection{Resource algebra}

A resource algebra (RA), represented by \(\Ra\), is an algebra consisting of
the underlying type \(T\),
the binary operation \((\cdot) \col T \X T \to T\),
the unit element \(\eps \col T\),\footnote{\
  Actually, Iris allows an RA to be non-unital,
  but here for simplicity we assume that every RA is unital.
}
the validity predicate \(\ok \col T \to \Prop\),
and the core-taking operation \(\abs{-} \col T \to T\),
satisfying the following rules.
\begin{gather*}
  (a \cdot b) \cdot c \,=\, a \cdot (b \cdot c) \qqquad
  a \cdot b \,=\, b \cdot a \qqquad
  a \cdot \eps \,=\, a \qqquad
  \ok(\eps) \br[.3em]
  a \itemle b \cand \ok (b) \imp \ok (a) \qqquad
  a \cdot \abs{a} = a \qqquad
  \abs{\abs{a}} = \abs{a} \qqquad
  a \itemle b \imp \abs{a} \itemle \abs{b} \br[.3em]
  \text{where}\qquad
  a \itemle b \,\defeq\,
    \ex{c}\, b = a \cdot c
\end{gather*}

This algebra is a partially commutative monoid (PCM) equipped with the core-taking operation \(\abs{-}\).
We can say that the core \(\abs{a}\) for an item \(a\) works like a unit element in the presence of \(a\).

The relation \(a \itemle b\) means that \(b\) has more resources than \(a\);
it is a preorder since the operation \((\cdot)\) is unital and associative.
The validity is antitone and the core is monotone over this preorder.

We use \(\prod\) for the big operator of \((\cdot)\).

We define the frame-preserving update relation \(a \itemupd B\) for \(a \col T\), \(B \col \pow T\) as follows.
\begin{equation*}
  a \itemupd B \ \defeq\
  \all{c \col T}\
  \ok(a \cdot c) \imp
  \ex{b \?\in\? B}\,
  \ok(b \cdot c)
\end{equation*}
It means that \(a\) can be changed into one of items in \(B\)
without invalidating the validity,
regardless of the other part of the resource \(c\).

For a resource algebra \(\Ra\),
we write \(\abs{\Ra}\) for its underlying type.

\subsubsection{OFEs and COFEs}

An ordered family of equivalences (OFE) is a type \(T\)
equipped with an infinite sequence of binary relations \((\Eq{-}) \col \NN \to T \X T \to \Prop\) (called the step-indexed equality),
satisfying
\begin{gather*}
  (\Eq{n})\ \text{is an equivalent relation} \qqqquad
  n \le m \cand x \Eq{m} y \imp x \Eq{n} y \br[.0em]
  x = y \iff (\all{n} x \Eq{n} y)
\end{gather*}
It can be regarded as a `step-indexed type'.
The step-indexed equality gets more refined as the step (the natural number index) increases,
and in the limit coincides with the actual equality.

A chain on an OFE \(T\), typed \(\Chain_T\), is an infinite sequence \(c \col \NN \to T\) satisfying \(n \le m \!\imp\! c_n \Eq{n} c_m\) for any \(n, m\).
A complete OFE (COFE) is an OFE \(T\) equipped with \(\lim \col \Chain_T \to T\) satisfying
\begin{equation*}
  c_n \Eq{n} \lim c
  \quad \note{c \col \Chain_T,\, n \col \NN}.
\end{equation*}

A step-indexed proposition \(X, Y\) is an antitone predicate over natural numbers, of the type
\begin{equation*}
  \SProp \,\defeq\,
    \subty{\phi \col \NN \!\to\! \Prop}{
      \all{n, m \st n \le m}\, \phi(m) \!\imp\! \phi(n)
    }.
\end{equation*}
It is a COFE, where we set \(X \Eq{n} Y \?\defeq\? \all{m \!\le\! n}\, X_m = Y_m\) and
\(\lim X \!\defeq\! \lam{n} X_n(n)\).

We say an OFE is discrete if its step-indexed equality is the actual equality for any step-index.
Any discrete OFE is complete (we can set \(\lim x \!\defeq\! x(0)\)).
We write \(\Delta T\) for the (unique) discrete COFE over the type \(T\).

We say a function \(f \col U \to T\) between two OFEs \(U, T\) is non-expansive,
if it satisfies \(\all{n, x, y}\, x \Eq{n} y \!\!\imp\!\! f(x) \Eq{n} f(y)\).
We write \(U \tone T\) for the type of non-expansive functions.
Also, we say \(f\) is contractive if it satisfies \(\all{n, x, y}\, (\all{m \!<\! n}\, x \Eq{m} y) \!\!\imp\!\! f(x) \Eq{n} f(y)\)
(i.e. \(\all{x, y}\, f(x) \Eq{0} f(y) \!\cand\! \all{n}\, x \Eq{n} y \!\imp\! f(x) \!\Eq{n + 1}\! f(y)\)).
The type \(U \tone T\) is an OFE, where we set \(f \Eq{n} g \defeq \all{x} f(x) \Eq{n} g(n)\);
also, it is a COFE when the codomain \(T\) is complete, since we can set
\(\lim f \!\defeq\! \lam{x} \lim\, (\lam{n} f_n(x))\).

The category of OFEs \(\OFE\) consists of any OFEs (of some universe) as objects and any
non-expansive functions between them as arrows.
The category of COFEs \(\COFE\) is the subcategory of \(\OFE\) with objects restricted to complete OFEs.
The categories \(\OFE\) and \(\COFE\) have any products.

We say a functor or profunctor over \(\OFE\) is locally non-expansive/contractive if its arrow mapping is non-expansive/contractive for any choice of the domain and the codomain.
Evidently,
locally non-expansive functors compose into a locally non-expansive functor,
and composition of a locally contractive functor and a locally non-expansive functor yields a contractive functor.

The delayed OFE \(\delay T\) for an OFE \(T\)
consists of the items of form \(\nextn x\) (for \(x \col T\))
and is equipped with the following step-indexed equality.
\begin{equation*}
  \nextn x \+\Eq{n}\+ \nextn y \ \defeq\
  n = 0 \cor x \!\Eq{n-1}\! y
\end{equation*}
Therefore, \(\delay\) forms a locally contractive functor over \(\OFE\).

The following fixed-point theorem holds.
\begin{theorem}[Fixed-Point Theorem over COFEs \cite{AmericaRutten}]\label{theorem:AmericaRutten}
  For any locally contractive profunctor \(F \col \COFE^\Op \X \COFE \to \COFE\)
  such that \(F (\Delta \Unit, \Delta \Unit)\) is inhabited,
  there exists a COFE \(T\) satisfying
  \(F(T, T) \simeq T\).
\end{theorem}

\subsubsection{Cameras}

As a step-indexed version of an RA, we define a camera.

A camera \(\Cmra\) is an algebra consisting of
the underlying OFE \(T\),
the binary operation \((\cdot) \col T \X T \tone T\),
the unit element \(\eps \col T\),\footnote{\
Again, Iris allows a camera to be non-unital,
but here for simplicity we assume that every camera is unital.
}
the step-indexed validity predicate \(\okd \col T \tone \SProp\), and
the core-taking operation \(\abs{-} \col T \tone T\),
satisfying the following rules.\footnote{\
  For the last rule, for constructiveness,
  we require that we have a function that constructs \(c, c'\) for each \(n, a, b, b'\).
}
\begin{gather*}
  (a \cdot b) \cdot c \,=\, a \cdot (b \cdot c) \qqquad
  a \cdot b \,=\, b \cdot a \qqquad
  a \cdot \eps \,=\, a \qqquad
  \ok(\eps) \br[.0em]
  a \itemle b \cand \Ok{n}(b) \imp \Ok{n}(a) \qqquad
  a \cdot \abs{a} = a \qqquad
  \abs{\abs{a}} = \abs{a} \qqquad
  a \itemle b \imp \abs{a} \itemle \abs{b} \br[.0em]
  \Ok{n}(a) \cand
  a \+\Eq{n}\+ b \cdot b' \imp
  \ex{c, c'}\
  a = c \cdot c' \cand
  b \Eq{n} c \cand b' \Eq{n} c' \br[.0em]
  \text{where}\qquad
  a \itemle b \,\defeq\,
    \ex{c}\, b = a \cdot c \qquad
  \Ok{n}(a) \defeq \okd(a)(n) \qquad
  \ok(a) \defeq \all{n} \Ok{n}(a)
\end{gather*}
We write \(a \Itemle{n} b\) for
\(\ex{c \col T}\, b \Eq{n} a \cdot c\).
We define \(a \itemupd B\) as \(\all{c \col T}\, \ok(a \cdot c) \!\imp\! \ex{b \?\in\? B}\, \ok(b \cdot c)\),
like we do for cameras.

We write \(\abs{\Cmra}\) for the underlying OFE of \(\Cmra\).

We say that a camera is discrete when its underlying COFE is discrete.
Any RA can be promoted into a discrete camera, by turning the underlying type into its discrete COFE and the validity predicate \(\ok\) into the step-indexed validity \(\lam{a} \lam{\_} \ok(a)\).
This forms a one-to-one correspondence between RAs and discrete cameras.

The category of cameras \(\Camera\) consists of
any cameras (of some universe) as objects and
any homomorphism over cameras as arrows;
here, a homomorphism is a map that is non-expansive, commutes with \((\cdot)\) and \(\abs{-}\), and preserves \(\okd\).

\subsubsection{Various RAs and cameras}

\paragraph{Agreement camera}

The agreement camera \(\AG(T)\) over an OFE \(T\) is defined as follows
(we use \(\hat X, \hat Y\) for objects typed \(\powfin T\) and \(X, Y\) for objects typed \(\abs{\AG(T)}\)).
\begin{gather*}
  \abs{\AG(T)} \defeq \powfin T \,/\, \equiv \quad
  \text{where}\quad
  \hat X \equiv \hat Y \defeq
    \all{n}\,
    \text{\((\Eq{n})\) is left- and right-total over \(\hat X \!\X\! \hat Y\)} \br[.1em]
  X \Eq{n} Y \defeq
    \text{\((\Eq{n})\) is left- and right-total over \(X \!\X\! Y\)} \qqquad
  X \cdot Y \defeq X \cup Y \qqquad
  \eps \defeq \emp \br[.0em]
  \Ok{n}(X) \defeq \all{x, y \in X}\, x \Eq{n} y \qqquad
  \abs{X} \defeq X
\end{gather*}
We write \(\ag x\) (where \(x \col T\)) for \(\{x\} \col \abs{\AG(T)}\).
The following properties hold.
\begin{gather*}
  \ok(\ag x) \qqquad
  \Ok{n}(\ag x \cdot \ag y) \imp
    x \Eq{n} y \qqquad
  \ag x \,\cdot\, \ag x \,=\, \ag x \qqquad
  \abs{\ag x} \,=\, \ag x
\end{gather*}

In this camera,
we can have any number of observations of the form \(\ag x\),
but they should coincide.
Because of the step indexing, the definition is a bit tricky.
When \(T\) is discrete, then \(\AG(T)\) simply forms an RA whose items are of form either \(\ag x\) or \(\eps\).

The function \(\AG\) forms a locally non-expansive functor from \(\OFE\) to \(\Camera\).

\paragraph{Exclusive camera}

The exclusive camera \(\EX(T)\) over an OFE \(T\) is defined as follows.
\begin{gather*}
  a \col \abs{\EX(T)} \sdef
    \eps \sor
    \exn x \ \ \note{x \col T} \sor
    \no \qquad
  a \Eq{n} b \defeq
    (\ex{x, y}\,
      a = \exn x \?\cand\?
      b = \exn y \?\cand\?
      x \Eq{n} y) \cor
    a = b \br[.0em]
  a \cdot b \defeq \begin{cases}
    a & b = \eps \\[-.5em]
    b & a = \eps \\[-.5em]
    \no & \text{otherwise}
  \end{cases} \qqquad
  \Ok{n}(a) \defeq a \ne \no \qqquad
  \abs{a} \defeq \eps
\end{gather*}
This is simply a camera where we can have up to one token of form \(\exn x\).

The following properties hold.
\begin{equation*}
  \ok(\exn x) \qqquad
  \neg \ok(\exn x \cdot \exn y) \qqquad
  \exn x \itemupd \{ \exn y \}
\end{equation*}

The function \(\EX\) forms a locally non-expansive functor on \(\Camera\).

\paragraph{Authoritative camera}

The authoritative camera \(\AUTH(\?\Cmra)\) over a camera \(\Cmra\) is defined as follows.
\begin{gather*}
  \abs{\AUTH(\?\Cmra)} \defeq
    \abs{\EX(\?\Cmra)} \X \abs{\Cmra} \qqquad
  (x, a) \Eq{n} (x', a') \defeq
    x \Eq{n} x' \cand x \Eq{n} a' \br[.3em]
  (x, a) \cdot (x', a') \defeq
    (x \cdot x',\, a \cdot a') \qqquad
  \eps \defeq (\eps, \eps) \qqquad
  \abs{(x, a)} \defeq (\eps, \abs{a}) \br[-.1em]
  \Ok{n}((x, b)) \defeq
    (x = \eps \cand \Ok{n}(b)) \cor
    (\ex{a \+\st\+ x = \exn a}\, \Ok{n}(a) \cand b \Itemle{n} a)
\end{gather*}
We write \(\prim a\) for \((\exn a, \eps)\) and \(\repl b\) for \((\eps, b)\)
(where \(a, b \col \abs{\Cmra}\)).
The following properties hold.
\begin{gather*}
  \Ok{n}(\prim a \cdot \repl b) \iff
    \Ok{n}(a) \cand b \Itemle{n} a \qqquad
  \repl a \,\cdot\, \repl b \,=\, \repl (a \cdot b) \qqquad
  \repl \eps \,=\, \eps \br[.3em]
  \neg \ok (\prim a \cdot \prim b) \qqquad
  \abs{\repl b} \,=\, \abs{\prim a \cdot \repl b} \,=\, \repl \abs{b}
\end{gather*}
We can have up to one token of form \(\prim a\).
When we have tokens \(\repl a\) and \(\repl b\), they are merged into \(\repl (a \cdot b)\).
When there are tokens \(\prim a\) and \(\repl b\),
we know that \(b\) is a part of \(a\).

The function \(\AUTH\) forms a locally non-expansive functor on \(\Camera\).

\paragraph{Product camera}

Given a family of cameras \(\Cmra\) indexed over a type \(T\),
we define the product camera \(\prod_{x \col T} \Cmra_x\) by pointwise lifting
(the underlying type is \(\prod_{x \col T} \abs{\Cmra_x}\)).
This forms the product functor in the category \(\Camera\).
We write \(T \to \Cmra\) for \(\prod_{\_ \col T} \Cmra\).

We use the following notations to write elements of the product camera.
\begin{equation*}
  \mapone{x}{a} \defeq
    \lam{y} \If y = x \Then a \Else \eps \qqquad
  \mapset{x}{a_x}{X} \defeq
    \lam{x} \If x \in X \Then a_x \Else \eps
\end{equation*}
For the first notation, we require that the condition \(\lam{y} y = x\) is decidable.
For the second notation, we require that \(X\) is a decidable set over \(T\).

Also, we introduce the finite-support map camera \((x \col T) \tofine \Cmra_x\),
which is the sub-camera of \(\prod_{x \col T} \Cmra_x\) of the following underlying type.
\begin{gather*}
  \slim
  \abs{(x \col T) \tofine \Cmra_x} \,\defeq\,
    \subty{\map \col \abs{\prod_{x \col T} \Cmra_x}}{\supp \map \ \text{is finite}} \br[.1em]
  \text{where}\quad
  \supp \map \defeq
    \set{x}{\map[x] \ne \eps}
\end{gather*}
It also forms a locally non-expansive functor over \(\Camera\).

\paragraph{Fraction RA}

The fraction RA \(\FRAC\) is defined as follows.
\begin{gather*}
  \abs{\FRAC} \defeq \Maybe(\QQf) \qqquad
  \ok(q^?) \defeq q^? \ne \no \br[.3em]
  q^? \cdot q'^? \defeq \begin{cases}
    q + q' & q^? = q,\ q'^? = q',\ q + q' \le 1 \\[-.3em]
    \no & \text{otherwise}
  \end{cases} \qqquad
  \eps \defeq 0 \qqquad
  \abs{q^?} \defeq 0
\end{gather*}

The zero-one RA \(\ZEROONE\) is the sub-RA of \(\FRAC\)
whose items are restricted to \(0\), \(1\) and \(\no\).

The fractional-ownership camera \(\FRACOWN(\Cmra)\) over a camera \(\Cmra\)
is the sub-camera of \(\FRAC \X \Cmra\)
whose items are restricted to the form \((0, \eps)\) or \((q, a)\) with \(q > 0\).
The function \(\FRACOWN\) forms a locally non-expansive functor on \(\Camera\).
