\subsection{Construction of the Iris Proposition}\label{index:iris-construct}

\subsubsection{Iris Proposition}

A pre-Iris proposition \(\hat P, \hat Q\) over the global camera \(\Cmra\)
is a monotone non-expansive map from \(\abs{\Cmra}\) to \(\SProp\), having the type
\begin{equation*}
  \PreIProp(\?\Cmra) \defeq \subty{
      X \col \abs{\Cmra} \tone \SProp
    }{
      \all{a, b, n \st a \Itemle{n} b}\,
      \all{m \!\le\! n}\,
      X(a)(m) \!\imp\! X(b)(m)
    }.
\end{equation*}
We define the equivalence over pre-Iris propositions as below.
It checks the equality only for valid elements at each step.
\begin{equation*}
  \hat P \equiv \hat Q \ \defeq\
    \all{n,\, a \st \Ok{n}(a)}\ \,
      \hat P(a)(n) = \hat Q(a)(n).
\end{equation*}

An Iris proposition \(P, Q, R\) over the global camera \(\Cmra\)
is an equivalence class of pre-Iris propositions (over \(\Cmra\)) with respect to the equivalence defined above,
having the quotient type
\begin{equation*}
  \FunIProp(\?\Cmra) \,\defeq\,
    \quotty{\PreIProp(\?\Cmra)}{\equiv}.
\end{equation*}
We represent an Iris proposition by its representative element.
We write \(\step{n}{P}(a)\) for \(P(a)(n)\).
Iris propositions form a COFE, by setting
\begin{equation*}
  P \Eq{n} Q \,\defeq\,
    \all{m \!\le\! n,\, a \st \Ok{m}(a)}\ \,
      \step{m}{P}(a) = \step{m}{Q}(a), \qqquad
  \lim P \defeq
    \lam{a} \lam{n}
      \step{n}{P_n}(a).
\end{equation*}
Moreover, \(\FunIProp\) forms a locally non-expansive functor from \(\Camera\) to \(\COFE\).

\subsubsection{Constructing the impredicative global camera}\label{index:pre-iris-impr}

The user of Iris can freely choose the type \(\CmraIdx\) for camera indices
and a \(\CmraIdx\)-indexed family \(\FunCmras\) of locally contractive profunctors typed \(\COFE^\Op \X \COFE \to \Camera\).\footnote{\
  In actual Iris, it is always a type of a finite set \(\{0, \ldots, n \?-\? 1\}\).
}
A ghost name \(\gamma\) is an element of the type \(\GhName \!\defeq\! \NN\).

Now we can construct a locally contractive profunctor \(\FunProdCmra \col \COFE^\Op \X \COFE \to \Camera\) as follows.
\begin{equation*}
  \slim
  \FunProdCmra(-^\Op, -) \,\defeq\,
  \slim \prod_{\cidx \col \CmraIdx}\,
  (\GhName \tofine
  \FunCmras_\cidx (-^\Op, -)).
\end{equation*}

We construct the COFE of the Iris proposition \(\IProp\)
so that the following recursive domain equation holds,
using \cref{theorem:AmericaRutten}.
\begin{equation*}
  \IProp \,\simeq\, \FunIProp(\FunProdCmra(\IProp, \IProp))
\end{equation*}
We equate \(\IProp\) with \(\FunIProp(\FunProdCmra(\IProp, \IProp))\).
We call \(\FunProdCmra(\IProp, \IProp)\) the global camera and write it as \(\GlCmra\).
We write \(\GlCmra_\cidx\) for \(\FunCmras_\cidx (\IProp, \IProp)\).

In this way, we can make the \emph{impredicative} global camera depend on the COFE of Iris propositions \(\IProp\),
as long as the reference to \(\IProp\) follows the local contractiveness condition.

For composability, the proof in Iris is made parametric in the choice of the global camera,
requiring only that specific cameras are stored at \emph{some} camera indices.
