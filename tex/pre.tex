\section{Basic Preliminaries}\label{index:pre}

A (pure) type, represented by \(T, U\), itself has the type \(\Type_n\), where the universe indicator \(n\) is often omitted.
A pure proposition, represented by \(\phi, \psi\), has the type \(\Prop\).
We write \(\top, \bot \col \Prop\) for the truth and the falsehood.

\paragraph{Subtypes}

We write \(\subty{x \col\? T \?}{\? \phi_x}\) for the subtype of the type \(T\) with the constraint that \(x \col T\) should satisfy \(\phi_x \col \Prop\).

\paragraph{Numbers}

We write \(\ZZ\) for the type of integers,
The type \(\NN\) represents natRal numbers (i.e. non-negative integers) and is a subtype of \(\ZZ\).
We write \(\QQ\) for the type of rational numbers.
We write \(\QQf\) for \(\subty{q \col \QQ}{0 \le q \le 1}\)
and \(\QQpf\) for \(\subty{q \col \QQ}{0 < q \le 1}\).

\paragraph{Maybe}

We introduce the type \(\Maybe T\),
whose item is either of form \(x\) (where \(x \col T\)) or \(\no\)
(we use \(\Maybe T\) only when \(T\) does not contain \(\no\)).
We often use variables with a superscript \(?\) for items of the type \(\Maybe T\)
(e.g. \(q^?\) for \(\Maybe \QQ\)).

\paragraph{Lists}

We write \(\List T\) for a list of items typed \(T\).
We write \((\cons)\) for the cons operator and \(\nil\) for the nil.
We write \([a_0, \ldots, a_{n-1}]\) for the list \(a_0 \cons \cdots \cons a_{n-1} \cons \nil\).
We write \(\len v\) for the length of a list \(v\).
We write \(v[i]\) for the \(i\)-th element of the list \(v\) for \(0 \le i < \len v\).

We introduce the following notations for writing a sub-list of a list \(v\).
\begin{gather*}
  v[i..j] \,\defeq\, [v[i], v[i \!+\! 1], \ldots, v[j \!-\! 1]]
  \quad \note{0 \le i \le j < \len v} \br[.1em]
  v[..j] \,\defeq\, v[0 .. j] \qqquad
  v[i..] \,\defeq\, v[i .. \len v]
\end{gather*}

We write \(T^n\) for the subtype \(\subty{v \col \List T}{\len v = n}\).

We write \(v \app w\) for the list obtained by concatenating \(v\) and \(w\).

\paragraph{Sequences}

By \(\seq{a}\), with an arrow on the top, we represent a sequence of items \(a_0, \dots, a_{n-1}\) of some length \(n\).
We sometimes write \(\seq{a}^n\) to clarify the length \(n\).
We write \(\eps\) for the empty sequence.

We introduce the notations for sub-sequences \(\seq{a}[i..j]\), \(\seq{a}[..j]\), \(\seq{a}[i..]\)
just as we did for sub-lists.

We can use the sequence notation for any repetition, with or without commas as a separator.
For example, we can write \(\Seq{v\, \cons}\, \nil\) for a list \(v_0 \cons \cdots \cons v_{n-1} \cons \nil\).

\paragraph{Sets}

The type \(\pow T\) represents a set of items typed \(T\), which is isomorphic to \(T \to \Prop\).
The type \(\powdec T\) represents a decidable set of items typed \(T\) (the membership on the set is decidable); it is isomorphic to \(T \to \Bool\).

The type \(\powfin T\) represents a finite set of items typed \(T\) (such that the equality over \(T\) is decidable).
It is isomorphic to the quotient type
\begin{equation*}
  \quotty{\List T}{
    (\lam{(\var{xs}, \var{ys})}\, \all{z}\, z \In \var{xs} \!\iff\! z \In \var{ys})
  }.
\end{equation*}
Any finite set is decidable.

We write \(A \disj B\) for \(A \cap B = \emp\) (i.e. \(A\) and \(B\) are disjoint).
We write \(A + B\) for the disjoint set union (i.e. \(A \cup B\) with the precondition \(A \disj B\))
and \(A - B\) for the proper set difference (i.e. \(A \diff B\) with the precondition \(B \sub A\)).

\paragraph{Maps}

The type \((x \col U) \pto T_x\) represents a partial map from \(x \col U\) to \(T_x\) (for \(U \col \Type,\, T \col U \!\to\! \Type\)).

The type \(\prod_{x \in A} T_x\) (for \(A \col \pow U\)) denotes a subtype of the partial-map type with the constraint that the domain equals \(A\);
we write \(T^A\) for \(\prod_{x \in A} T\).
The type \((x \col U) \ptofin T_x\) (for \(A \col \pow U\)) denotes a subtype of the partial-map type with the constraint that the domain is finite.
We regard the total-map type \((x \col U) \to T_x\) as a subtype of the partial-map type with the constraint that the domain equals the universal set over \(U\).

For a map \(\map \col (x \col U) \pto T_x\), we introduce the following notations.
We write \(\dom \map \col \pow U\) for the domain of the map.
We write \(\map(a)\) (or sometimes \(\map_a\)) for the value of the map at \(a \in \dom \map\);
it returns \(\no\) for \(a \notin \dom \map\).
We write \(\map \{a \by v\} \col (x \col U) \pto T_x\) for the map obtained from \(\map\) by adding/updating the value \(v \col T_a\) at the key \(a \col U\).
We write \(\map \{a \by \no\} \col (x \col U) \pto T_x\) for the map obtained from \(\map\) by removing the key-value pair at \(a \col U\) if \(a \in \dom \map\).
We write \(\map \{ a_i \by v_i \}_{i \in I}\) for the map obtained from \(\map\) by adding/updating the value \(v_i\) at \(a_i\) for every \(i \in I\)
(for \(I \col \pow U'\), \(v \col U' \?\!\to\! T\), and an injective map \(a \col U' \?\!\to\! U\)).
We write \(\map \{ a_i \by \no \}_{i \in I}\) for the map obtained from \(\map\) by removing the key-value pair at \(a_i\) if \(a_i \in \dom \map\) for every \(i \in I\)
(for \(I \col \pow U'\), \(a \col U' \?\!\to\! U\)).
