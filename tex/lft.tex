\section{Lifetime Logic}\label{index:lft}

Lifetime logic \cite{RustBelt,RustBeltAppx} is well described in \cite{RalfPhd}.
We don't explain the model here.

We have the following properties on lifetimes.
\begin{gather*}
  (\alpha \tand \beta) \tand \gamma \,=\,
    \alpha \tand (\beta \tand \gamma) \qqquad
  \alpha \tand \beta \,=\,
    \beta \tand \alpha \qqquad
  \alpha \tand \eps \,=\, \alpha
\end{gather*}

Lifetime inclusion is defined as follows.
\begin{equation*}
  \alpha \tle \beta \,\defeq\,
    (\all{q} [\alpha]_q \acsrf{\iN_\lft}\! q'.\ [\beta]_{q'}) \,\sep\,
    \pers ([\dagger \beta] \wandupdf{\iN_\lft} [\dagger \alpha])
\end{equation*}
We have the following properties on lifetime inclusion.
\begin{gather*}
  \persistent(\alpha \tle \beta) \br[.2em]
  \alpha \tle \alpha \qqquad
  \alpha \tle \beta \,\sep\,
  \beta \tle \gamma \yields
    \alpha \tle \gamma \qqquad
  \alpha \tle \beta \,\sep\,
  \alpha \tle \gamma \biyields
    \alpha \tle \beta \tand \gamma
\end{gather*}

We have the following properties on lifetime tokens.
\begin{gather*}
  \Ctx_\lft
  \!\!\yields\!\!
    \updf{\iN_\lft}\!
    \ex{\alpha}\
  [\alpha]_1 \,\sep\,
  \pers(\, [\alpha]_1 \wandlaterupdFf{\iN_\lftuser}{\iN_\lft + \iN_\lftuser} [\dagger \alpha] \,)
  \newrule{Lft-Intro} \br[.1em]
  [\alpha]_{q + q'} \,\biyields\,
    [\alpha]_q \,\sep\, [\alpha]_{q'} \qqquad
  [\alpha \tand \beta]_q \,\biyields\,
    [\alpha]_q \,\sep\, [\beta]_q \br[.1em]
  \yields\, [\eps]_1 \qqquad
  [\dagger \eps] \yields \bot \qqquad
  [\alpha]_q \,\sep\, [\dagger \alpha] \yields \bot \qqquad
  [\dagger (\alpha \!\tand\! \beta)] \biyields
    [\dagger \alpha] \sep [\dagger \beta]
\end{gather*}

We have the following properties on full references.
\begin{gather*}
  \later P
    \ \,\sep\! \Ctx_\lft
    \!\!\yields\!\!
      \updf{\iN_\lft}\
    \reffull{\alpha} P \,\sep\,
    ([\dagger \alpha] \wandupdf{\iN_\lft} \later P)
  \newrule{FullRef-Intro} \br[.3em]
  \Ctx_\lft
  \!\!\yields\
    \reffull{\alpha} P \,\sep\, [\alpha]_q
      \ \acsrf{\iN_\lft}\, \later P
  \newrule{FullRef-Access} \br[.3em]
  \beta \tle \alpha \,\sep\, \reffull{\alpha} P
    \,\yields\, \reffull{\beta} P
  \newrule{FullRef-Mono-Lft} \br[.3em]
  \beta \tle \alpha \,\sep\,
  \reffull{\alpha} P
    \ \,\sep\! \Ctx_\lft
    \!\!\yields\!\!
      \updf{\iN_\lft}\,\
    \reffull{\beta} P \,\sep\,
    \bigl(\,
      [\dagger \beta] \wandupdf{\iN_\lft}
      \reffull{\alpha} P
    \,\bigr)
  \newrule{FullRef-Reborrow} \br[.3em]
  \reffull{\alpha} \reffull{\beta} P
    \ \,\sep\! \Ctx_\lft
    \!\!\yields\!\!
      \laterupdf{\iN_\lft}\
    \reffull{\alpha \tand \beta} P
  \newrule{FullRef-Unnest} \br[.3em]
  \reffull{\alpha} (P \sep Q)
    \ \,\sep\! \Ctx_\lft
    \!\!\yields\!\!
      \updf{\iN_\lft}\
    \reffull{\alpha} P \,\sep\,
    \reffull{\alpha} Q
  \newrule{FullRef-Split} \br[.3em]
  \reffull{\alpha} P \,\sep\,
  \reffull{\alpha} Q
    \ \,\sep\! \Ctx_\lft
    \!\!\yields\!\!
      \updf{\iN_\lft}\
    \reffull{\alpha} (P \sep Q)
  \newrule{FullRef-Merge} \br[.3em]
  \reffull{\alpha} (\ex{x} P_x)
    \ \,\sep\! \Ctx_\lft
    \!\!\yields\!\!
      \updf{\iN_\lft}\,\
    \ex{x}\? \reffull{\alpha} P_x
  \newrule{FullRef-Freeze} \br[.3em]
  (\,
    \later P \,\wandupdf{\iN_\lft}\,
      \later Q \sep
      (\later Q \wandupdf{\iN_\lftuser} \later P) \sep
      R
  \,) \,\sep\,
  \reffull{\alpha} P \,\sep\, [\alpha]_q
    \ \,\sep\! \Ctx_\lft
    \!\!\yields\!\!
      \updf{\iN_\lft}\,\
    \reffull{\alpha} Q \,\sep\,
    R \,\sep\,
    [\alpha]_q
  \newrule{FullRef-Modify}
\end{gather*}

We have the following properties on fractured references.
\begin{gather*}
  \persistent(\reffrac{\alpha} P) \br[.3em]
  \reffull{\alpha} P(1)
    \ \,\sep\! \Ctx_\lft
    \!\!\yields\!\!
      \updf{\iN_\lft}\,\
    \reffrac{\alpha} P
  \newrule{FullRef-FracRef} \br[.3em]
  \pers \bigl(\,
    \all{q_0, q_1}\,
    P(q_0 + q_1) \liff P(q_0) \sep P(q_1)
  \,\bigr) \,\sep\,
  \reffrac{\alpha} P
    \ \,\sep\! \Ctx_\lft \!\! \yields\
    [\alpha]_q \,\acsrf{\iN_\lft}\! q'.\ \later P(q')
  \newrule{FracRef-Access} \br[.3em]
  \beta \tle \alpha \,\sep\,
  \reffrac{\alpha} P \,\yields\,
    \reffrac{\beta} P
  \newrule{FracRef-Lft-Mono} \br[.3em]
  \pers \later (\all{q}\, P(q) \liff P'(q)) \,\sep\,
  \reffrac{\alpha} P \,\yields\,
    \reffrac{\alpha} P'
  \newrule{FracRef-Modify}
\end{gather*}
