Import EqNotations.
Require Import ClassicalDescription Equality.
From stdpp Require Import strings propset.
From iris.algebra Require Import auth cmra functions gmap csum frac agree.
From iris.bi Require Import fractional.
From iris.proofmode Require Import tactics.
From iris.base_logic Require Import invariants.
From iris_string_ident Require Import ltac2_string_ident.
From lrust.util Require Import util.

Open Scope util_scope.

Declare Scope proph_scope.
Delimit Scope proph_scope with proph.
Open Scope proph_scope.

(** * Pointed Type *)

Record ptType: Type := PtType { pt_ty: Type; pt_val: pt_ty }.
Coercion pt_ty: ptType >-> Sortclass.

Implicit Type Ap Bp: ptType.

(** We use classical axioms only here. *)
Global Instance pt_type_eq_dec : EqDecision ptType :=
  { decide_rel _ _ := excluded_middle_informative _ }.

(** * Basic Notions *)

Record proph_var := ProphVar { pv_ty: ptType; pv_id: positive }.
Notation "$( Ap , i )" := (ProphVar Ap i)
  (at level 2, format "$( Ap ,  i )") : proph_scope.
Notation "x .ty" := (x.(pv_ty)) (at level 2, format "x .ty") : proph_scope.
Notation "x .id" := (x.(pv_id)) (at level 2, format "x .id") : proph_scope.

Global Instance proph_var_eq_dec : EqDecision proph_var.
Proof. solve_decision. Qed.

Definition proph_var_set := propset proph_var.
Definition proph_var_list := list proph_var.
Definition proph_var_list_to_set
  : proph_var_list → proph_var_set := set_to_propset.
Coercion proph_var_list_to_set: proph_var_list >-> proph_var_set.

Implicit Type (x y: proph_var) (X Y: proph_var_set) (xs ys : proph_var_list).

Definition proph_asn := ∀ x: proph_var, x.ty.

Global Instance proph_asn_inhabited : Inhabited proph_asn :=
  populate (λ π, π.ty.(pt_val)).

Implicit Type π: proph_asn.

(** * Prophecy Dependency *)

Definition proph_asn_eqv X π π' := ∀x, x ∈ X → π x = π' x.
Notation "π .≡{ X }≡ π'" := (proph_asn_eqv X π π')
  (at level 70, format "π  .≡{ X }≡  π'") : proph_scope.

Definition proph_dep {A} (v: _ → A) X := ∀ π π', π .≡{ X }≡ π' → v π = v π'.
Notation "v ./ X" := (proph_dep v X)
  (at level 70, format "v  ./  X") : proph_scope.

(** ** Lemmas *)

Lemma proph_dep_one x : (.$ x) ./ {[x]}.
Proof. move=> ?? Eqv. by apply Eqv. Qed.

Lemma proph_asn_eqv_mono X Y π π' : X ⊆ Y → π .≡{ Y }≡ π' → π .≡{ X }≡ π'.
Proof. move=> Sub Eqv ??. by apply Eqv, Sub. Qed.

Lemma proph_dep_mono {A} X Y (v : _ → A) : X ⊆ Y → v ./ X → v ./ Y.
Proof. move=> ? Dep ???. by apply Dep, (proph_asn_eqv_mono _ Y). Qed.

Lemma proph_dep_constr {A B} (f: A → B) v X : v ./ X → f ∘ v ./ X.
Proof. move=> Dep ?? /Dep ?. by apply (f_equal f). Qed.

Lemma proph_dep_constr2 {A B C} (f: A → B → C) v w X :
  v ./ X → w ./ X → f ∘ v ⊛ w ./ X.
Proof.
  move=> Dep Dep' ?? Eqv. move: (Eqv) (Eqv)=> /Dep ? /Dep' ?.
  by apply (f_equal2 f).
Qed.

Lemma proph_dep_destr {A B} f `{Inj A B (=) (=) f} v X :
  f ∘ v ./ X → v ./ X.
Proof. move=> Dep ?? /Dep ?. by apply (inj f). Qed.

Lemma proph_dep_destr2 {A B C} f `{Inj2 A B C (=) (=) (=) f} v w X :
  f ∘ v ⊛ w ./ X → v ./ X ∧ w ./ X.
Proof. move=> Dep. split; move=> ?? /Dep ?; by edestruct (inj2 f). Qed.

(** * Prophecy Log *)

Record proph_log_item :=
  ProphLogItem { pli_pv: proph_var; pli_val: proph_asn → pli_pv.ty }.
Local Notation ".{ x := v }" := (ProphLogItem x v)
  (at level 2, format ".{ x  :=  v }") : proph_scope.
Local Notation "pli .pv" := (pli.(pli_pv))
  (at level 2, format "pli .pv") : proph_scope.
Local Notation "pli .val" := (pli.(pli_val))
  (at level 2, format "pli .val") : proph_scope.

Local Definition proph_log := list proph_log_item.

Implicit Type L: proph_log.

Local Definition proph_log_unres L x := Forall (λ pli, x ≠ pli.pv) L.
Local Notation "x .# L" := (proph_log_unres L x)
  (at level 70, format "x  .#  L") : proph_scope.
Local Notation "{# L #}" := (PropSet (proph_log_unres L))
  (at level 10, format "{# L #}") : proph_scope.

Local Fixpoint proph_log_ok L := match L with
| [] => True
| .{x:=v} :: L' => x .# L' ∧ v ./ {#L#} ∧ proph_log_ok L'
end.
Local Notation ".✓ L" := (proph_log_ok L)
  (at level 20, format ".✓  L") : proph_scope.

Local Definition proph_sat_log π L := Forall (λ pli, π pli.pv = pli.val π) L.
Local Notation "π ◁ L" := (proph_sat_log π L)
  (at level 70, format "π  ◁  L") : proph_scope.

(** ** Satisfiability *)

Local Definition proph_upd x v π : proph_asn := λ y,
  match decide (x = y) with left eq => rew eq in v π | right _ => π y end.
Local Notation ":<[ x := v ]>" := (proph_upd x v)
  (at level 5, format ":<[ x  :=  v ]>") : proph_scope.

Local Lemma proph_upd_lookup π x v : :<[x:=v]> π x = v π.
Proof. rewrite /proph_upd. case (decide (x = x))=> [?|?]; by [simpl_eq|]. Qed.
Local Lemma proph_upd_lookup_ne π x v y : x ≠ y → :<[x:=v]> π y = π y.
Proof. rewrite /proph_upd. by case (decide (x = y))=> [?|?]. Qed.

Local Fixpoint proph_modify π L := match L with
| [] => π
| .{x:=v} :: L' => proph_modify (:<[x:=v]> π) L'
end.
Local Notation "π ! L" := (proph_modify π L)
  (at level 30, format "π  !  L") : proph_scope.

Local Lemma proph_modify_eqv L : ∀π, π ! L .≡{{#L#}}≡ π.
Proof.
  elim L=> [|[??]?] /=; [done|] => Eqv ?? /elem_of_PropSet FA.
  inversion FA. rewrite Eqv; [|done]. by apply proph_upd_lookup_ne.
Qed.

Local Lemma proph_ok_modify_sat L : .✓ L → ∀π, π ! L ◁ L.
Proof.
  rewrite /proph_sat_log. elim L=> [|[x v] L'] /=; [done|].
  move=> IH [In [? /IH ?]] ?. apply Forall_cons. split; [|done].
  rewrite proph_modify_eqv; [|by apply In]. rewrite proph_upd_lookup.
  clear L. set L := .{x:=v} :: L'. have Dep': v ./ {#L#} by done.
  symmetry. apply Dep', (proph_modify_eqv L).
Qed.

Local Lemma proph_ok_sat L : .✓ L → ∃π, π ◁ L.
Proof.
  move=> Val. exists (inhabitant ! L). by apply proph_ok_modify_sat.
Qed.

(** * Prophecy Camera *)

Implicit Type q: Qp.

Local Definition proph_itemR Ap :=
  csumR fracR (agreeR (leibnizO (proph_asn → Ap))).
Local Definition proph_gmapUR Ap := gmapUR positive (proph_itemR Ap).
Local Definition proph_smryUR := discrete_funUR proph_gmapUR.
Definition prophUR := authUR proph_smryUR.

Implicit Type S: proph_smryUR.

Local Definition aitem {Ap} v : proph_itemR Ap := Cinr (to_agree v).
Local Definition fitem {Ap} q : proph_itemR Ap := Cinl q.
Local Definition line x it : proph_smryUR := .{[x.ty := {[x.id := it]}]}.
Local Definition add_line x it S : proph_smryUR :=
  .<[x.ty := <[x.id := it]> (S x.ty)]> S.

Definition prophΣ := #[GFunctor prophUR].
Class prophPreG Σ := ProphPreG { proph_preG_inG :> inG Σ prophUR }.
Class prophG Σ := ProphG { proph_inG :> prophPreG Σ; proph_name : gname }.
Instance subG_prophPreG {Σ} : subG prophΣ Σ → prophPreG Σ.
Proof. solve_inG. Qed.

Definition prophN := nroot .@ "proph".

(** * Iris Propositions *)

Local Definition proph_sim_smry_log S L :=
  ∀Ap i v, S Ap !! i ≡ Some (aitem v) ↔ .{$(Ap,i):=v} ∈ L.
Local Notation "S :~ L" := (proph_sim_smry_log S L)
  (at level 70, format "S  :~  L") : proph_scope.

Implicit Type (φc ψc : proph_asn → Prop) (φ ψ : Prop).

Section defs.
Context `{!invG Σ, !prophG Σ}.

(** Prophecy Context *)
Local Definition proph_inv: iProp Σ :=
  ∃ S, ⌜∃L, .✓ L ∧ S :~ L⌝ ∗ own proph_name (● S).
Definition proph_ctx: iProp Σ := inv prophN proph_inv.

(** Prophecy Token *)
Definition proph_tok x q : iProp Σ :=
  own proph_name(◯ line x (fitem q)).
Definition proph_toks xs q : iProp Σ := [∗ list] x ∈ xs, proph_tok x q.

(** Prophecy Observation *)
Local Definition proph_atom pli : iProp Σ :=
  own proph_name (◯ line pli.pv (aitem pli.val)).
Definition proph_obs φc : iProp Σ :=
  ∃L, ⌜∀π, π ◁ L → φc π⌝ ∗ [∗ list] pli ∈ L, proph_atom pli.

End defs.

Notation "q :[ x ]" := (proph_tok x q)
  (at level 2, format "q :[ x ]") : bi_scope.
Notation "q :+[ xs ]" := (proph_toks xs q)
  (at level 2, format "q :+[ xs ]") : bi_scope.
Notation "⟨ π , φ ⟩" := (proph_obs (λ π, φ%type%stdpp))
  (π name, at level 5, format "⟨ π ,  φ ⟩") : bi_scope.

(** * Iris Lemmas *)

Section lemmas.
Context `{!invG Σ, !prophG Σ}.

(** Instances *)

Global Instance proph_ctx_persistent : Persistent proph_ctx := _.

Global Instance proph_tok_timeless q x : Timeless q:[x] := _.
Global Instance proph_tok_fractional x : Fractional (λ q, q:[x]%I).
Proof.
  move=> ??. by rewrite -own_op -auth_frag_op
    discrete_fun_singleton_op singleton_op -Cinl_op.
Qed.
Global Instance proph_tok_as_fractional q x :
  AsFractional q:[x] (λ q, q:[x]%I) q.
Proof. split; by [|apply _]. Qed.

Global Instance proph_toks_timeless q xs : Timeless q:+[xs] := _.
Global Instance proph_toks_fractional xs : Fractional (λ q, q:+[xs]%I) := _.
Global Instance proph_toks_as_fractional q xs :
  AsFractional q:+[xs] (λ q, q:+[xs]%I) q.
Proof. split; by [|apply _]. Qed.

Global Instance proph_obs_persistent φc : Persistent ⟨π, φc π⟩ := _.
Global Instance proph_obs_timeless φc : Timeless ⟨π, φc π⟩ := _.

(** Initialization *)

Lemma proph_init `{!prophPreG Σ} E :
  ↑prophN ⊆ E → ⊢ |={E}=> ∃ _: prophG Σ, proph_ctx.
Proof.
  move=> ?. iMod (own_alloc (● ε)) as (γ) "Own"; [by apply auth_auth_valid|].
  set IProphG := ProphG Σ _ γ. iExists IProphG.
  iMod (inv_alloc _ _ proph_inv with "[Own]") as "?"; [|done]. iModIntro.
  iExists ε. iSplitR; [|done]. iPureIntro. exists []. split; [done|] => ???.
  rewrite lookup_empty. split; move=> Hyp; inversion Hyp.
Qed.

(** Taking a Fresh Prophecy Variable *)

Lemma proph_intro {Ap} E (I: gset positive) :
  ↑prophN ⊆ E → proph_ctx ={E}=∗ ∃i, ⌜i ∉ I⌝ ∗ 1:[$(Ap,i)].
Proof.
  iIntros (?) "?". iInv prophN as (S) "> [%OkSim Auth]".
  case OkSim=> L [? Sim].
  case (exist_fresh (I ∪ dom _ (S Ap)))
    => [i /not_elem_of_union [? /not_elem_of_dom EqNone]].
  set x := $(Ap,i). set S' := add_line x (fitem 1) S.
  iMod (own_update _ _ (● S' ⋅ ◯ line x (fitem 1)) with "Auth") as "[Auth' ?]".
  { by apply auth_update_alloc,
      discrete_fun_insert_local_update, alloc_singleton_local_update. }
  iModIntro. iSplitL "Auth'"; last first. { iModIntro. iExists i. by iSplit. }
  iModIntro. iExists S'. iSplitR; [|done]. iPureIntro. exists L.
  split; [done|] => Bp j ?. rewrite /S' /add_line /discrete_fun_insert -Sim.
  case (decide (Ap = Bp))=> [?|?]; [|done]. subst. simpl.
  case (decide (i = j))=> [<-|?]; [|by rewrite lookup_insert_ne].
  rewrite lookup_insert EqNone.
  split; move=> Eqv; [apply (inj Some) in Eqv|]; inversion Eqv.
Qed.

(** Prophecy Resolution *)

Local Lemma proph_tok_unres S L x q :
  S :~ L → own proph_name (● S) -∗ q:[x] -∗ ⌜x .# L⌝.
Proof.
  move=> Sim. iIntros "Auth Tok".
  iDestruct (own_valid_2 with "Auth Tok") as %ValBoth. iPureIntro.
  move: ValBoth=> /auth_both_valid_discrete [Inc _].
  apply Forall_forall. move=> [[Bp ?] ?] /Sim Eqv Eq.
  move/(discrete_fun_included_spec_1 _ _ Bp) in Inc.
  rewrite /line Eq discrete_fun_lookup_singleton in Inc.
  move: Eqv. move: Inc=> /singleton_included_l [? [-> Inc]].
  move=> Eqv. apply (inj Some) in Eqv. move: Inc. rewrite Eqv.
  move=> /Some_csum_included [|[[?[?[_[?]]]]|[?[?[?]]]]]; done.
Qed.

Local Lemma proph_tok_ne x y q : 1:[x] -∗ q:[y] -∗ ⌜x ≠ y⌝.
Proof.
  iIntros "Tok Ptok". iDestruct (own_valid_2 with "Tok Ptok") as %ValBoth.
  iPureIntro=> ?. subst. move: ValBoth.
  rewrite -auth_frag_op auth_frag_valid discrete_fun_singleton_op
    discrete_fun_singleton_valid singleton_op singleton_valid -Cinl_op
    Cinl_valid. apply exclusive_l, _.
Qed.

Lemma proph_resolve E x v ys q : ↑prophN ⊆ E → v ./ ys →
  proph_ctx -∗ 1:[x] -∗ q:+[ys] ={E}=∗ ⟨π, π x = v π⟩ ∗ q:+[ys].
Proof.
  move: x v => [Ap i] v. set x := $(Ap,i). iIntros (??) "? Tok Ptoks".
  iInv prophN as (S) "> [%OkSim Auth]". case OkSim=> L [? Sim].
  iDestruct (proph_tok_unres with "Auth Tok") as %UrX; [done|].
  set L' := .{x := v} :: L. iAssert ⌜∀y, y ∈ ys → y .# L'⌝%I as %UrYs.
  { iIntros (? In).
    iDestruct (big_sepL_elem_of with "Ptoks") as "Ptok"; [apply In|].
    iDestruct (proph_tok_ne with "Tok Ptok") as %?.
    iDestruct (proph_tok_unres with "Auth Ptok") as %?; [done|].
    iPureIntro. by constructor. }
  set S' := add_line x (aitem v) S.
  iMod (own_update_2 _ _ _ (● S' ⋅ ◯ line x (aitem v)) with "Auth Tok")
    as "[Auth' #?]".
  { apply auth_update, discrete_fun_singleton_local_update_any,
      singleton_local_update_any => ? _. by apply exclusive_local_update. }
  iModIntro. iSplitL "Auth'"; last first.
  { iModIntro. iSplitR; [|done]. iExists [.{x:=v}]. rewrite big_sepL_singleton.
    iSplitR; [|done]. iPureIntro=> ? Sat. by inversion Sat. }
  iModIntro. iExists S'. iSplitR; [|done]. iPureIntro. exists L'. split.
  { split; [done| split; [|done]]. apply (proph_dep_mono ys); [|done] => ??.
    rewrite elem_of_PropSet. by apply UrYs. }
  have InLNe y w : .{y:=w} ∈ L → x ≠ y.
  { move/Forall_forall in UrX. by move=> /UrX ?. }
  move=> Bp j ?. rewrite elem_of_cons. case (decide (x = $(Bp,j)))=> [Eq|?].
  { inversion Eq. subst.
    rewrite /S' /add_line discrete_fun_lookup_insert lookup_insert. split.
    - move=> Eqv. left. apply (inj (Some ∘ aitem)) in Eqv. by rewrite Eqv.
    - move=> [Eq'|/InLNe ?]; by [dependent destruction Eq'|]. }
  have Eqv : S' Bp !! j ≡ S Bp !! j.
  { rewrite /S' /add_line /discrete_fun_insert.
    case (decide (Ap = Bp))=> [?|?]; [|done]. simpl_eq.
    case (decide (i = j))=> [?|?]; by [subst|rewrite lookup_insert_ne]. }
  rewrite Eqv Sim. split; [by right|].
  move=> [Eq|?]; by [dependent destruction Eq|].
Qed.

(** Operations on Prophecy Observations *)

Lemma proph_obs_trivial φ : φ → ⊢ ⟨_, φ⟩.
Proof. move=> ?. iExists []. by iSplit. Qed.

Lemma proph_obs_weaken φc ψc : (∀π, φc π → ψc π) → ⟨π, φc π⟩ -∗ ⟨π, ψc π⟩.
Proof.
  move=> Wkn. iIntros "Obs". iDestruct "Obs" as (L) "[%SatImp ?]".
  iExists L. iSplitR; [|done]. iPureIntro=> ??. by apply Wkn, SatImp.
Qed.

Lemma proph_obs_merge φc ψc : ⟨π, φc π⟩ -∗ ⟨π, ψc π⟩ -∗ ⟨π, φc π ∧ ψc π⟩.
Proof.
  iIntros "Obs Obs'". iDestruct "Obs" as (L) "[%SatImp Atoms]".
  iDestruct "Obs'" as (L') "[%SatImp' Atoms']". iExists (L ++ L').
  iSplitR; [|by rewrite big_sepL_app; iCombine "Atoms Atoms'" as "?"].
  iPureIntro=> ? /Forall_app [??]. split; by [apply SatImp|apply SatImp'].
Qed.

Lemma proph_obs_sat E φc :
  ↑prophN ⊆ E → proph_ctx -∗ ⟨π, φc π⟩ ={E}=∗ ⌜∃π₀, φc π₀⌝.
Proof.
  iIntros (?) "? Obs". iDestruct "Obs" as (L') "[%SatImp #Atoms]".
  iInv prophN as (S) "> [%OkSim Auth]". case OkSim=> L [Ok Sim].
  move: (Ok)=> /proph_ok_sat [π /Forall_forall Sat]. iModIntro.
  iAssert ⌜π ◁ L'⌝%I as %?; last first.
  { iSplitL; last first. { iPureIntro. exists π. by apply SatImp. }
    iModIntro. iExists S. iSplitR; [|done]. iPureIntro. by exists L. }
  rewrite /proph_sat_log Forall_forall. iIntros ([[Ap i] v] In). simpl.
  iAssert (proph_atom .{$(Ap,i):=v}) with "[Atoms]" as "Atom".
  { iApply big_sepL_elem_of; by [apply In|]. }
  iDestruct (own_valid_2 with "Auth Atom") as %ValBoth. simpl in ValBoth.
  iPureIntro. apply (Sat .{$(Ap,i):=v}), Sim.
  move: ValBoth=> /auth_both_valid_discrete [Inc Val].
  move/(discrete_fun_included_spec_1 _ _ Ap) in Inc.
  rewrite /line discrete_fun_lookup_singleton in Inc.
  move: Inc=> /singleton_included_l [it [Eqv /Some_included[->|Inc]]]; [done|].
  rewrite Eqv. constructor.
  apply (lookup_valid_Some _ i it) in Val; [|done]. move: Val.
  move: Inc=> /csum_included [->|[[?[?[?]]]|[?[?[Eq[-> Inc]]]]]]; [done|done|].
  move=> Val. move: Inc. move: Val=> /Cinr_valid/to_agree_uninj [? <-].
  inversion Eq. by move/to_agree_included <-.
Qed.

End lemmas.
