\subsection{Invariants}\label{index:iris-invariant}

Hereafter, we equip Iris with the mechanism of invariants.

\subsubsection{World}

An invariant name \(\iota\) is an element of the type \(\InvName \!\defeq\! \List \NN\).
For invariant names, we write \(\eps\) for \(\nil\)
and write \(a_0.a_1.\cdots.a_n\) for \([a_0, a_1, \cdots, a_n]\).

A mask \(\iE\) is a decidable set of invariant names, typed \(\powdec \InvName\).
We write \(\top\) for the universal set on \(\InvName\).

A namespace \(\iN\) is an element of the type \(\InvName\),
but used differently from invariant names.
We write \(\up \iN\) for the mask \(\{\, \iN \app \iota \mid \iota \col \InvName \,\}\)
(note that this is an infinite set).
We simply write \(\iN\) instead of \(\up \iN\) when it is clearly used as a mask.

for a decidable infinite set of invariant names, which we call a namespace.\footnote{\
  In actual Iris, \(\iN\) has some structure,
  but we can just regard it as a decidable infinite set of invariant names.
}

We register to the global camera the following cameras for invariants, \(\INV\), \(\DIS\) and \(\EN\).
Note that the camera \(\INV\) is dependent on \(\later \IProp\);
it is made possible by the machinery described in \cref{index:pre-iris-impr}.
\begin{gather*}
  \INV \defeq \AUTH(\InvName \tofine \AG(\delay \IProp)) \br[.0em]
  \DIS \defeq \InvName \tofine \ZEROONE \qqquad
  \EN \defeq \InvName \to \ZEROONE
\end{gather*}
We use some fixed ghost names \(\gamma_\INV\), \(\gamma_\DIS\) and \(\gamma_\EN\) for the cameras
and introduce the following notations.
\begin{gather*}
  [I]_\INV \defeq
    \res{
      \prim\, \mapset{\iota}{\ag\, (\nextn\, I_\iota)}{\dom I}
    }{\gamma_\INV}{\INV}
  \quad \note{I \col \InvName \ptofin \IProp} \br[.3em]
  [\iota]_\DIS \defeq
    \res{\mapone{\iota}{1}}{\gamma_\DIS}{\DIS} \qqquad
  [\iE]_\EN \defeq
    \res{\mapset{\iota}{1}{\iE}}{\gamma_\EN}{\EN}
\end{gather*}
The world resource \(\World \col \IProp\) is defined as follows.
\begin{equation*}
  \World \,\defeq\,
    \ex{I \col \InvName \?\ptofin\? \IProp}\, [I]_\INV \sep
    \slim \bigsep_{\iota \+\in\+ \dom I}\, (\later I_\iota \sep [\iota]_\DIS \ \lor\ [\{\iota\}]_\EN)
\end{equation*}
In the world, there are a finite number of Iris propositions with some distinct invariant names,
which are registered by the resource \([I]\).
For each invariant \(I_\iota\), the world either owns the resource \(\later I_\iota\) and the token \([\iota]_\DIS\) or owns the token \([\{\iota\}]_\EN\) instead.
When we have the token \([\{\iota\}]_\EN\),
by temporally depositing it we can take out \(\later I_\iota\) and \([\iota]_\DIS\) from the world,
and take back \([\{\iota\}]_\EN\) by storing back \(\later I_\iota\) and \([\iota]_\DIS\).

\subsubsection{Fancy update}

The fancy update modality is defined as follows.
\begin{equation*}
  \updff{\iE}{\iE'} P \,\defeq\,
    \World \sep [\iE]_\EN \ \wand\
    \upd \exzero\, (\World \sep [\iE']_\EN \sep P)
\end{equation*}
It can be read as follows:
after inputting the world and the token \([\iE]_\EN\) and performing frame update,
the resource outputs \(P\) with the world and the token \([\iE']_\EN\), under the \(\exzero\) modality.
We write \(\updf{\iE} P\) for \(\updff{\iE}{\iE} P\).
The operators \(\updff{\iE}{\iE'}\) and \(\updf{\iE}\) have the weakest precedence just as \(\upd\).
The following properties hold.
\begin{gather*}
  P \yields Q \ \imp\
    \updff{\iE}{\iE'} P \yields \updff{\iE}{\iE'} Q \qqquad
  P \yields
    \updff{\iE + \iE'}{\iE} \updff{\iE}{\iE + \iE'} P \br[.0em]
  \updff{\iE}{\iE'} \updff{\iE'}{\iE''} P \yields
    \updff{\iE}{\iE''} P \qqquad
  \updff{\iE}{\iE'} P \yields \updff{\iE + \iE''}{\iE' + \iE''} P \qqquad
  P \,\sep\, \updff{\iE}{\iE'} Q \yields
    \updff{\iE}{\iE'} (P \sep Q) \br[.0em]
  \upd P \yields \updf{\emp} P \qqquad
  \exzero \updff{\iE}{\iE'} \exzero P \biyields \updff{\iE}{\iE'} P
\end{gather*}
To prove the properties above,
we can use the weak commutativity \(\exzero \upd P \yields \upd \exzero P\)
and the monadic properties of the update modality \(\upd\) and the except-0 modality \(\exzero\).

We use the following notation for convenience,
which we call a view shift.
\begin{equation*}
  P \wandupdff{\iE}{\iE'} Q \,\defeq\,
    P \wand \updff{\iE}{\iE'} Q
\end{equation*}
Again, we write \(P \wandupdf{\iE} Q\)
for \(P \wandupdff{\iE}{\iE} Q\).
The operators \(\wandupdff{\iE}{\iE'}\) and \(\wandupdf{\iE}\) have the same precedence as \(\wand\).
The following properties hold.
\begin{gather*}
  (P \wandupdff{\iE}{\iE'} Q) \,\sep\,
  (Q \wandupdff{\iE'}{\iE''} R) \,\yields\,
    P \wandupdff{\iE}{\iE''} R \qqquad
  P \wandupdff{\iE}{\iE'} Q \,\yields\,
    P \sep R \,\wandupdff{\iE + \iE''}{\iE' + \iE''}
    Q \sep R \br[.0em]
  (P \wandupdff{\iE}{\iE'} Q) \,\sep\,
  (P' \wandupdff{\iE'}{\iE''} Q')
    \,\yields\,
    P \sep P' \,\wandupdff{\iE}{\iE''}\, Q \sep Q' \br[.0em]
  P \wandupdff{\iE}{\iE'} \exzero Q \,\biyields\,
  P \wandupdff{\iE}{\iE'} Q \,\biyields\,
  \exzero P \wandupdff{\iE}{\iE'} Q \qqquad
  P \wand Q \,\yields\, P \wandupdf{\emp} Q
\end{gather*}

We also use the following notations,
in order to describe fancy updates with an intermediate later modality.
\begin{equation*}
  \laterupdFf{\iE'}{\iE} P \,\defeq\,
    \updff{\iE}{\iE'} \later \updff{\iE'}{\iE} P \qqquad
  P \wandlaterupdFf{\iE'}{\iE} Q \,\defeq\,
    P \wand \laterupdFf{\iE'}{\iE} Q
\end{equation*}
Also, we write \(\laterupdf{\iE} P\) for \(\laterupdFf{\iE}{\iE} P\) and
\(P \wandlaterupdf{\iE} Q\) for \(P \wandlaterupdFf{\iE}{\iE} Q\).

\subsubsection{Accessor pattern}

We introduce the following persistent resource, called the accessor pattern.
\begin{equation*}
  P \ \acsrff{\iE}{\iE'}\! x.\ \ Q_x \,\defeq\,
    \pers\, \bigl(\,
      P \ \,\wandupdff{\iE}{\iE'}\!
      \ex{x}\,\
      Q_x \,\sep\, (Q_x \?\wandupdff{\iE'\?}{\iE}\? P)
    \,\bigr)
\end{equation*}
It means that we can temporarily get \(Q_x\) for some \(x\) out of \(P\), temporarily shifting the mask into \(\iE'\) instead of \(\iE\).
We write \(P \acsrf{\iE}\! x.\ Q_x\) for \(P \acsrff{\iE}{\iE}\! x.\ Q_x\).
We write \(P \acsrff{\iE}{\iE'} Q\) and \(P \acsrf{\iE} Q\)
for \(P \acsrff{\iE}{\iE'}\?\! \_.\ Q\) and \(P \acsrf{\iE}\! \_.\ Q\).
The accessor operator has the weakest precedence (the same as the universal and existential quantifiers).
Putting it plainly, we can use the accessor pattern as follows.
\begin{equation*}
  (P \,\acsrff{\iE}{\iE'}\! x.\ Q_x) \,\sep\,
  P
  \ \,\yields\!\! \updff{\iE}{\iE'} \ex{x}\,\
  Q_x \,\sep\,
  (Q_x \?\wandupdff{\iE'}{\iE}\? P)
\end{equation*}
The accessor pattern satisfies the following properties.
\begin{equation*}
  \persistent(P \acsrff{\iE}{\iE'}\! x.\ Q_x) \qqquad
  P \acsrff{\iE}{\iE'}\! x.\ \exzero Q_x \,\biyields\,
  P \acsrff{\iE}{\iE'}\! x.\ Q_x \,\biyields\,
  \exzero P \acsrff{\iE}{\iE'}\! x.\ Q_x
\end{equation*}

\subsubsection{Invariant token}

Now we define the invariant token \(\inv{P}{\iE}\) as follows.
\begin{equation*}
  \inv{P}{\iE} \,\defeq\,
    \top \acsrff{\iE}{\emp} \later P
\end{equation*}
Unfolding the accessor pattern, it turns out to be equivalent to
\begin{equation*}
  \pers \updff{\iE}{\emp} \bigl(\,
    \later P \,\sep\, (\later P \,\wandupdff{\emp}{\iE} \top)
  \,\bigr).
\end{equation*}
It is a persistent resource that
can take out \(\later P\) from the world by giving the token \([\iE]_\EN\) to the world
and can get back \([\iE]_\EN\) by returning \(\later P\).
Putting it plainly, we can use the invariant tokens as follows.
\begin{equation*}
  \inv{P}{\iE}
    \ \yields\!\! \updff{\iE}{\emp}\,\
    \later P \,\sep\,
    (\later P \wandupdff{\emp}{\iE}\? \top) \qqquad
  \timeless(P) \ \imp\
  \inv{P}{\iE}
    \ \yields\!\! \updff{\iE}{\emp}\,\
    P \,\sep\,
    (P \wandupdff{\emp}{\iE}\? \top)
\end{equation*}

We can get an invariant token \(\inv{P}{\iN}\) by consuming \(\later P\).
\begin{equation*}
  \later P \ \,\yields\!\! \updf{\emp}\ \
    \inv{P}{\iN}
  \newrule{Inv-Intro}
\end{equation*}

\begin{proof}[Proof of \cref{rule:Inv-Intro}]
  First, we newly register to the world the Iris proposition \(P\) at a fresh name \(\iota\),
  storing \(\later P\),
  and obtain a persistent observation that \(\iota\) refers to \(\nextn P\),
  which is summarized as the following lemma.
  \begin{equation*}
    \later P
    \ \,\yields\!\! \updf{\emp} \ex{\iota \?\in\? \iN}\ \
      \res{\repl\, \mapone{\iota}{\ag (\nextn P)}}{\gamma_\INV}{\INV}
  \end{equation*}
  Now we have the token \(\res{\repl\, \mapone{\iota}{\ag (\nextn P)}}{\gamma_\INV}{\INV}\),
  which is persistent.
  Now it suffices to prove
  \begin{equation*}
    \res{\repl\, \mapone{\iota}{\ag (\nextn P)}}{\gamma_\INV}{\INV}
      \ \,\yields\!\! \updff{\iN}{\emp}\,\
      \later P \,\sep\,
      (\later P \wandupdff{\emp}{\iN} \top).
  \end{equation*}
  We can prove it using the following sequents.
  \begin{gather*}
    \res{\repl\, \mapone{\iota}{\ag (\nextn P)}}{\gamma_\INV}{\INV}
      \ \yields\!\! \updff{\iN}{\emp}\,\
      \later P \,\sep\, [\iota]_\DIS \,\sep\, [\iN \?-\? \{\iota\}]_\EN \br[.1em]
    [\iota]_\DIS \,\sep\, [\iN \?-\? \{\iota\}]_\EN \,\sep\, \later P
      \ \yields\!\! \updff{\emp}{\iN}\ \top
  \end{gather*}
  Using the persistent token, by consuming \([\{\iota\}]_\EN\),
  we can take out from the world the resource \(\later P\) and the token \([\iota]_\DIS\).
  After we put back \(\later P\) and \([\iota]_\DIS\) to the world,
  we get back \([\{\iota\}]_\EN\).
\end{proof}

The invariant token satisfies the following properties.
\begin{gather*}
  \persistent(\inv{P}{\iE}) \qqquad
  (\later P \acsrff{\iE'}{\emp} \later Q) \ \sep\
  \inv{P}{\iE} \,\yields\,
    \inv{Q}{\iE + \iE'} \br[.0em]
  \inv{P}{\iE} \sep \inv{Q}{\iE'} \yields
    \inv{P \sep Q}{\iE + \iE'} \qqquad
  \inv{P \sep Q}{\iE} \yields
    \inv{P}{\iE} \sep\,
    \inv{Q}{\iE} \qqquad
  \inv{P}{\iE} \yields \inv{P}{\iE + \iE'}
\end{gather*}

\subsubsection{Non-atomic invariants}

A thread id \(\xt\) is an object of the type \(\ThId \!\defeq\! \GhName\).

We introduce the following RA for non-atomic invariants.
\begin{equation*}
  \NA \,\defeq\, (\ThId \tofine \ZEROONE) \,\X\, (\ThId \to \ZEROONE)
\end{equation*}
We introduce the following notions.
\begin{equation*}
  \vwith{\step{n}{Q}}{
    [\xt.\iE]_\na \,\defeq\,
      \res{(\eps, \mapset{\iota}{1}{\iE})}{\xt}{\NA} \qqquad
    \nainv{P}{\xt.\iE} \,\defeq\,
      \ex{\iota \in \iE}\, \inv{
        P \sep
        \res{(\mapone{\iota}{1}, \eps)}{\xt}{\NA} \ \lor\
        [\xt.\iE]_\na
      }{\iE}
  }
\end{equation*}
The token \([\xt.\iE]_\na\) claims the right to access non-atomic invariants at the thread id \(\xt\), in the mask \(\iE\).
The persistent token \(\nainv{P}{\xt.\iE}\) informs that
we have a non-atomic invariant for the resource \(\later P\) at the thread id \(\xt\), in the mask \(\iE\).

The following properties hold.
\begin{gather*}
  \nainv{P}{\xt.\iE} \ \yields\
    [\xt.\iE]_\na \,\acsrf{\iE}\, \later P \qqquad
  \timeless(P) \ \imp\
  \nainv{P}{\xt.\iE} \ \yields\
    [\xt.\iE]_\na \,\acsrf{\iE}\, P \br[.2em]
  \yields\!\! \upd \ex{\xt}\ \
    [\xt.\top]_\na \qqquad
  [\xt.(\iE \?+\? \iE')]_\na \biyields
    [\xt.\iE]_\na \,\sep\, [\xt.\iE']_\na \br[.0em]
  \later P \ \,\yields\!\! \updf{\emp}\ \
    \nainv{P}{\xt.\iN} \qqquad
  \persistent(\nainv{P}{\xt.\iE})
\end{gather*}
