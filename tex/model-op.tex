\subsection{Various operations}\label{index:model-op}

\subsubsection{Mutable borrow}

The resource \(\xw \isbor{\alpha}{\xt} \tau \rfn{v}\) for data borrowed until a lifetime \(\alpha\) ends is defined as follows.
\begin{align*}
&
  \xw \isbor{\alpha}{\xt} \tau \rfn{v} \,\defeq\,
    \all{q}\,
    [\dagger \alpha] \,\sep\,
    [\tau.\lft]_q \ \wand {} \br[-.2em]
& \hspace{3em}
    \Wp{ \tau.\scan(\xw) }{
      \ex{v'}\,
      \know{\lam{\pi} v_\pi \?=\? v'_\pi} \,\sep\,
      \xw \is{\xt} \tau \rfn{v'} \,\sep\,
      [\tau.\lft]_q
    }{ \iN_\lft + \iN_\pry }
\end{align*}
It means that,
when we know that the lifetime \(\alpha\) has ended and that the lifetime \(\tau.\lft\) is still ongoing,
after performing the scan,
we get the data-owning resource \(\xw \is{\xt} \tau \rfn{v'}\),
for some value \(v'\) that agrees with \(v\) for every valid \(\pi\).
The following properties hold.
\begin{gather*}
  \alpha \tle \beta \,\sep\,
  \xw \isbor{\alpha}{\xt} \tau \rfn{v}
    \,\yields\,
    \xw \isbor{\beta}{\xt} \tau \rfn{v}
  \newrule{BorData-Lft-Mono} \br[.0em]
  \know{\lam{\pi} v_\pi = v'_\pi} \,\sep\,
  \xw \isbor{\alpha}{\xt} \tau \rfn{v}
    \,\yields\,
    \xw \isbor{\alpha}{\xt} \tau \rfn{v'}
  \newrule{BorData-Value-Modify}
\end{gather*}

We have the following lemma for mutable borrow.
\begin{equation*}
  \xl \is{\xt} \rown_n \tau \rfn{v}
    \ \ \sep\! \Ctx_\lft
    \!\sep\! \Ctx_\pry
    \!\!\yields\!\!
      \updf{\iN_\lft}\!
      \ex{w}\ \
    \xl \is{\xt} \refmut{\alpha} \tau \rfn{\lam{\pi}\! (v_\pi, w_\pi)} \ \sep\
    \xl \isbor{\alpha}{\xt} \rown_n \tau \rfn{w}
  \newrule{MutRef-Borrow}
\end{equation*}
Here, we get some prophecy value \(w\), which is shared by the second component of the pure value of the mutable reference and the pure value of the borrowed ownership pointer.

\begin{proof}[Proof of \cref{rule:MutRef-Borrow}]
  By \cref{rule:MutObs-PryCtrl-Intro},
  we take a fresh prophecy variable \(x\) satisfying \(\PryTy(x.\tidn) = \tau.\type\),
  and get a mutation observer \(\MutObs_x(v)\) and a prophecy control \(\PryCtrl(x, v)\).
  We set \(w \!\defeq\! \lam{\pi} \pi(x)\).
  We name the following proposition.
  \begin{equation*}
    P \,\defeq\, \ex{\xws', v'}\,
      \xl \mto \xws' \,\sep\,
      \xws' \is{\xt} \tau \rfn{v'} \,\sep\,
      \PryCtrl(x, v')
  \end{equation*}

  The resource \(\xl \is{\xt} \rown_n \tau \,\{ v \}\) is decomposed into
  \(\Dealloc(\xl, \abs{\tau}, n)\), \(\xl \mto \xws\) and \(\later\, \xws \is{\xt} \tau \rfn{v}\) for some \(\xws\).
  By \cref{rule:FullRef-Intro},
  we perform the following update.
  \begin{equation*}
    \xl \mto \xws \,\sep\,
    \later\, \xws \is{\xt} \tau \rfn{v} \,\sep\,
    \later\, \PryCtrl(x, v)
      \ \sep\! \Ctx_\lft
      \!\!\yields\!\!
        \updf{\iN_\lft}\,\
      \reffull{\alpha} P \,\sep\,
      ([\dagger \alpha] \wandupdf{\iN_\lft} \later P)
  \end{equation*}

  From \(\MutObs_x(v)\) and \(\reffull{\alpha} P\),
  we get \(\xl \is{\xt} \refmut{\alpha} \tau \,\{ \lam{\pi}\? (v_\pi, w_\pi) \}\).

  From \([\dagger \alpha] \wandupdf{\iN_\lft} \later P\) and \(\Dealloc(\xl, \abs{\tau}, n)\),
  we can construct \(\xl \isbor{\alpha}{\xt} \rown_n \tau \,\{ w \}\) using the following.
  \begin{align*}
  &
    \Ctx_\pry
    \!\!\yields\,
    \hoare{
      \xl \mto \xws' \,\sep\,
      \later\, \xws' \is{\xt} \tau \rfn{v'} \,\sep\,
      \later\, \PryCtrl(x, v') \,\sep\,
      [\tau.\lft]_q
    }{ \br[-.3em]
  & \hspace{4em} \scanat_\tau(\xl) \,}{
      \xl \mto \xws' \,\sep\,
      \xws' \is{\xt} \tau \rfn{v'} \,\sep\,
      \know{\lam{\pi} \pi(x) = v'_\pi} \,\sep\,
      [\tau.\lft]_q
    }{ \iN_\pry }
  \end{align*}
  Here, we get a prophecy assertion \(\know{\lam{\pi} \pi(x) = v'_\pi}\) from the prophecy control \(\PryCtrl(x, v')\) by \cref{rule:PryCtrl-Resolver} and \cref{rule:Resolver-ResolveUp}.
  By performing the scan, we can get a relevant partial prophecy token,
  by \cref{rule:Ty-Own-Pry} on \(\tau\).
\end{proof}

\subsubsection{Mutable reborrow}

We have the following lemma for reborrowing a mutable reference.
\begin{equation*}
  \begin{aligned}
  &
    \beta \tle \alpha \ \sep\
    \xl \is{\xt} \refmut{\alpha} \tau \rfn{\lam{\pi}\? (v_\pi, w_\pi)}
      \ \ \sep\! \Ctx_\lft
      \!\!\yields\!\!
        \updf{\iN_\lft}\!
        \ex{w'} \\[-.3em]
  & \hspace{1em}
      \xl \is{\xt} \refmut{\beta} \tau \rfn{\lam{\pi}\? (v_\pi, w'_\pi)} \ \sep\
      \xl \isbor{\beta}{\xt} \refmut{\alpha} \tau \rfn{\lam{\pi}\? (w'_\pi, w_\pi)}
  \end{aligned}
  \newrule{MutRef-Reborrow}
\end{equation*}
Like \cref{rule:MutRef-Borrow},
we get some prophecy value \(w'\),
which is shared by the second component of the pure value of the new mutable reference and the first component of the pure value of the reborrowed mutable reference.
Note that the reborrowed mutable reference retains the second component of the pure value.

\begin{proof}[Proof of \cref{rule:MutRef-Reborrow}]
  Let \(x\) be the prophecy variable for the input mutable reference.
  We have \(\all{\pi} w_\pi = \pi(x)\).

  ...
\end{proof}

\subsubsection{Update on mutable references}

We have the following lemma for updating the target of a mutable reference.
\begin{equation*}
  \begin{aligned}
  &
    \xl \is{\xt} \refmut{\alpha} \tau \rfn{\lam{\pi}\? (v_\pi, w_\pi)} \ \sep\
    [\alpha]_q
      \ \ \sep\! \Ctx_\lft\!\! \yields\!\! \updf{\iN_\lft}\! \ex{\xws}\,\ \
      \xl \mto \xws \ \sep\
      \later\, \xws \is{\xt} \tau \rfn{v} \ \sep {} \\[-.3em]
  & \hspace{4em}
      \bigl(\, \all{\xws', v'}\,\
        \xl \mto \xws' \ \sep\
        \later\, \xws' \is{\xt} \tau \rfn{v'}
          \ \wandupdf{\iN_\lft}\
          \xl \is{\xt} \refmut{\alpha} \tau \rfn{\lam{\pi}\? (v'_\pi, w_\pi)} \ \sep\
          [\alpha]_q
      \,\bigr)
  \end{aligned}
  \newrule{MutRef-Update}
\end{equation*}

\begin{proof}[Proof of \cref{rule:MutRef-Update}]
  Let \(x\) be the prophecy variable for the input mutable reference;
  it satisfies \(\all{\pi} w_\pi = \pi(x)\).
  We access the full reference of the mutable reference by \cref{rule:FullRef-Access}
  and obtain the following (with the help of \cref{rule:MutObs-PryCtrl-Agree}), for some \(\xws\).
  \begin{align*}
  &
    \MutObs_x(v) \,\sep\,
    \xl \mto \xws \,\sep\,
    \later\, \xws \is{\xt} \tau \rfn{v} \,\sep\,
    \PryCtrl(x, v) \,\sep {} \br[-.2em]
  & \hspace{1em}
    \bigl(\, \all{\xws', v'}\
      \MutObs_x(v') \,\sep\,
      \xl \mto \xws' \,\sep\,
      \later\, \xws' \is{\xt} \tau \rfn{v'} \,\sep\,
      \PryCtrl(x, v') \br[-.4em]
  & \hspace{12em}
      \wandupdf{\iN_\lft}\,
      \xl \is{\xt} \refmut{\alpha} \tau
        \rfn{\lam{\pi}\? (v'_\pi, \pi(x))} \,\sep\,
      [\alpha]_q \,\bigr)
  \end{align*}
  Using the wand obtained by \cref{rule:MutObs-PryCtrl-Update},
  we get the expected resource.
\end{proof}

\subsubsection{Releasing and weakening mutable references}

We have the following lemmas for releasing and weakening a mutable reference resolving the prophecy variable.
\begin{gather*}
  \begin{aligned}
  &
    \Ctx_\lft \!\sep\! \Ctx_\pry
    \!\!\yields\
    \hoare{
      \xl \is{\xt} \refmut{\alpha} \tau \rfn{v} \,\sep\,
      [\alpha \!\tand\! \tau.\lft]_q
    }{\, \scanat_\tau(\xl) \\[-.2em]
  & \hspace{7em} }{
      \know{\lam{\pi} v_\pi.\fut = v_\pi.\cur} \,\sep\,
      [\alpha \!\tand\! \tau.\lft]_q
    }{ \iN_\lft + \iN_\pry }
  \end{aligned}
  \newrule{MutRef-Release} \br[.0em]
  \begin{aligned}
  &
    \Ctx_\lft \!\sep\! \Ctx_\pry
    \!\!\yields\
    \hoare{
      \xl \is{\xt} \refmut{\alpha} \tau \rfn{v} \,\sep\,
      [\alpha \!\tand\! \tau.\lft]_q
    }{\, \scanat_\tau(\xl);\, \scanat_\tau(\xl) \\[-.2em]
  & \hspace{1em} }{
      \know{\lam{\pi} v_\pi.\fut = v_\pi.\cur} \,\sep\,
      \xl \is{\xt} \rown_n \tau \rfn{\lam{\pi} v_\pi.\cur} \,\sep\,
      [\alpha \!\tand\! \tau.\lft]_q
    }{ \iN_\lft + \iN_\pry }
  \end{aligned}
  \newrule{MutRef-Weaken}
\end{gather*}

\begin{proof}[Proof of \cref{rule:MutRef-Release}]
  Let \(x\) be the prophecy variable for the mutable reference;
  it satisfies \(\all{\pi} v_\pi.\fut = \pi(x)\).
  We access the full reference of the mutable reference with the help of the lifetime token \([\alpha]_q\) by \cref{rule:FullRef-Access}.
  Then, by \cref{rule:MutObs-PryCtrl-Resolve},
  consuming the mutation observer,
  we get the prophecy assertion \(\know{\lam{\pi} v_\pi.\fut = v_\pi.\cur}\).
  We get a relevant partial prophecy token after performing the scan,
  with the help of the lifetime token \([\tau.\lft]_q\),
  by \cref{rule:Ty-Own-Pry} on \(\tau\).
  The prophecy control is retained,
  which is put back to the full reference of the mutable reference.
\end{proof}

\begin{proof}[Proof of \cref{rule:MutRef-Weaken}]
  Just like the proof of \cref{rule:MutRef-Release},
  consuming the mutation observer,
  we get the prophecy assertion \(\know{\lam{\pi} v_\pi.\fut = v_\pi.\cur}\),
  performing the first scan to take a relevant partial prophecy token.
  Then we convert the full reference into a data-sharing resource performing the second scan, by \cref{rule:Ty-Own-Shr}.
\end{proof}

\subsubsection{Splitting mutable references}

We have the following lemmas for splitting mutable references to a pair and a variant.
\begin{gather*}
  \begin{aligned}
  &
    \xl \is{\xt} \refmut{\alpha} (\tau_0 \?\X\? \tau_1) \rfn{\lam{\pi}\? (v_\pi, w_\pi)} \,\sep\,
    [\alpha]_q
      \ \ \sep\! \Ctx_\lft \!\sep\! \Ctx_\pry
      \!\!\yields\!\!
        \updf{\iN_\lft + \iN_\pry}\!
        \ex{w_0, w_1}\,\
        [\alpha]_q \,\sep {} \\[-.2em]
  & \hspace{1em}
      \know{\lam{\pi}\, w_\pi = (w_{0\, \pi}, w_{1\, \pi})} \,\sep\,
      \xl \is{\xt} \refmut{\alpha} \tau_0
        \rfn{\lam{\pi}\? (v_\pi.0, w_{0\, \pi})} \,\sep\,
      \xl \?+\? \abs{\tau_0} \is{\xt} \refmut{\alpha} \tau_1
        \rfn{\lam{\pi}\? (v_\pi.1, w_{1\, \pi})}
  \end{aligned}
  \newrule{MutRef-Split-Pair} \br[-.2em]
  \begin{aligned}
  &
    \xl \is{\xt} \refmut{\alpha} (\tau_0 \?+\? \tau_1)
      \rfn{\lam{\pi}\? (\inj_i v'_\pi, w_\pi)} \,\sep\,
    [\alpha]_q
      \ \ \sep\! \Ctx_\lft \!\sep\! \Ctx_\pry
      \!\!\yields\!\!
        \updf{\iN_\lft + \iN_\pry}\!
        \ex{w'} \\[-.1em]
  & \hspace{5.5em}
      \know{\lam{\pi}\, w_\pi = \inj_i (w'_\pi)} \,\sep\,
      \xl \!+\! 1 \is{\xt} \refmut{\alpha} \tau_i
        \rfn{\lam{\pi}\? (v'_\pi, w'_\pi)} \,\sep\,
      [\alpha]_q
  \end{aligned}
  \newrule{MutRef-Split-Sum}
\end{gather*}

\begin{proof}[Proof of \cref{rule:MutRef-Split-Pair}]
  Let \(x\) be the prophecy variable for the given mutable reference.
  We have \(\all{\pi} \pi(x) = w_\pi\).
  Let \(v_i\) be \(\lam{\pi} v_\pi.i\) for \(i = 0, 1\).
  Also, let \(\xl_0\) be \(\xl\) and \(\xl_1\) be \(\xl + \abs{\tau}\).

  By \cref{rule:PryVar-Intro},
  we take fresh prophecy variables \(y_0, y_1\) for the output mutable references
  and get mutation observers \(\MutObs_{y_i}(v_i)\) and prophecy controls \(\PryCtrl(y_i, v_i)\) for \(i = 0, 1\).
  We set \(w_i \!\defeq\! \lam{\pi} \pi(y_i)\) for \(i = 0, 1\).
  We name the following proposition.
  \begin{equation*}
    P \,\defeq\,
      \all{v'_0, v'_1}\
      \Resolver\bigl(\+
        \lam{\pi}\! (\pi(y_0), \pi(y_1)),\,
        \lam{\pi}\! (v'_{0\, \pi}, v'_{1\, \pi})
      \+\bigr) \,\wand\,
      \PryCtrl\bigr(\+
        x,\, \lam{\pi}\? (v'_{0\, \pi}, v'_{1\, \pi})
      \+\bigr)
  \end{equation*}

  Now we transform the full reference of the input mutable reference into
  the full references for the output mutable references,
  acquiring the prophecy assertion \(\know{\lam{\pi} \pi(x) = (\pi(y_0), \pi(y_1))}\).
  We use \cref{rule:FullRef-Modify}, \cref{rule:FullRef-Split}, and the following sequents.
  \begin{gather*}
    \xl \mto \xws_0 \!\app\! \xws_1 \,\sep\,
    \later\, \xws_0 \!\app\! \xws_1 \is{\xt} \tau_0 \X \tau_1 \rfn{v}
      \,\yields\,
      \slim \bigsep_{i < 2}\,
      \bigl(\,
        \xl_i \mto \xws_i \,\sep\,
        \later\, \xws_i \is{\xt} \tau_i \rfn{v_i}
      \,\bigr) \br[.2em]
    \begin{aligned}
    & \slim
      \MutObs_x(v) \,\sep\,
      \PryCtrl(x, v) \,\sep\,
      \bigsep_{i < 2}\, [y_i]_1
        \ \ \sep\! \Ctx_\pry
        \!\!\yields\!\!
          \updf{\iN_\pry} \\[-.2em]
    & \slim \hspace{6em}
        \know{\lam{\pi} \pi(x) = (\pi(y_0), \pi(y_1))} \,\sep\,
        \bigsep_{i < 2}\, [y_i]_1 \,\sep\,
        P
    \end{aligned} \br[.2em]
    \begin{aligned}
    & \slim
      \bigsep_{i < 2}\,
      \bigl(\,
        \xl_i \mto \xws'_i \,\sep\,
        \later\, \xws'_i \is{\xt} \tau_i \rfn{v'_i} \,\sep\,
        \later\, \PryCtrl(y_i, v'_i)
      \,\bigr) \,\sep\,
      P
        \ \ \sep\! \Ctx_\pry
        \!\!\yields {} \\[-.2em]
    & \hspace{2em}
        \xl \mto \xws'_0 \!\app\! \xws'_1 \,\sep\,
        \later\, \xws'_0 \!\app\! \xws'_1 \is{\xt} \tau_0 \X \tau_1 \rfn{\lam{\pi}\? (v'_{0\, \pi}, v'_{1\, \pi})} \,\sep\,
        \later\, \PryCtrl(x,\, \lam{\pi}\? (v'_{0\, \pi}, v'_{1\, \pi}))
    \end{aligned}
  \end{gather*}
  The second sequent follows from \cref{rule:MutObs-PryCtrl-PreResolve}.
  We temporarily obtain prophecy tokens \(\bigsep_{i < 2}\, [y_i]_1\) for the second sequent by \cref{rule:MutObs-PryCtrl-Token}.
  For the third sequent,
  we turn the input prophecy controls into resolvers \(\Resolver(\lam{\pi}\? \pi(y_i),\, v'_i)\) (for \(i = 0, 1\)) by \cref{rule:PryCtrl-Resolver},
  merge them into \(\Resolver(
    \lam{\pi}\! (\pi(y_0), \pi(y_1)),\,
    \lam{\pi}\! (v'_{0\, \pi}, v'_{1\, \pi})
  )\) by \cref{rule:Resolver-Transform},
  and finally apply \(P\) to get the prophecy control \(\PryCtrl(
    x,\, \lam{\pi}\? (v'_{0\, \pi}, v'_{1\, \pi})
  )\).
  The third sequent generates the view shift under \(\iN_\lftuser\) required by \cref{rule:FullRef-Modify}.

  Combining the full references obtained above and the mutation observers \(\MutObs_{y_i}(v_i)\) we have created,
  we finally acquire the expected output mutable references.
\end{proof}

\begin{proof}[Proof of \cref{rule:MutRef-Split-Sum}]
  Similar to \cref{rule:MutRef-Split-Pair}.
\end{proof}

\subsubsection{Operations on shared references}

We have the following lemma for a shared borrow.\footnote{\
  We can also perform a shared borrow by the combination of \cref{rule:MutRef-Borrow} and \cref{rule:MutRef-Weaken}.
}
\begin{equation*}
  \Ctx_\lft \!\!\yields\
  \hoare{
    \xl \is{\xt} \rown_n \tau \rfn{v}
  }{ \scanat_\tau(\xl) }{
    \xl \is{\xt} \refshr{\alpha} \tau \rfn{v} \,\sep\,
    \xl \isbor{\alpha}{\xt} \rown_n \tau \rfn{v}
  }{ \iN_\lft }
  \newrule{ShrRef-Borrow}
\end{equation*}

\begin{proof}[Proof of \cref{rule:ShrRef-Borrow}]
  The resource \(\xl \is{\xt} \rown_n \tau \rfn{v}\) is decomposed into
  \(\Dealloc(\xl, \abs{\tau}, n)\), \(\xl \mto \xws\) and \(\later\, \xws \is{\xt} \tau \rfn{v}\) for some \(\xws\).
  We make a full reference \(\reffull{\alpha}\, (\xl \mto \xws \,\sep\, \xws \is{\xt} \tau \rfn{v})\)
  and a view shift \([\dagger \alpha] \,\wandupdf{\iN_\lft} \xl \mto \xws \,\sep\, \later \xws \is{\xt} \tau \rfn{v}\)
  by \cref{rule:FullRef-Intro}.

  Combining the view shift with \(\Dealloc(\xl, \abs{\tau}, n)\), we get \(\xl \isbor{\alpha}{\xt} \rown_n \tau \rfn{v}\).
  Performing the scan, we turn the full reference into \(\xws \isshr{\alpha}{\xt} \tau \rfn{v}\) by \cref{rule:Ty-Own-Shr} on \(\tau\).
  Combining it with \(\xl \mto \xws\),
  we get a shared reference.
\end{proof}

We have the following lemmas for splitting shared references.
\begin{gather*}
  \xl \is{\xt} \refshr{\alpha} (\tau_0 \?\X\? \tau_1) \rfn{v}
    \ \yields\
    \xl \is{\xt} \refshr{\alpha} \tau_0
      \rfn{\lam{\pi} v_\pi.0} \,\sep\,
    \xl \!+\! \abs{\tau_0} \is{\xt} \refshr{\alpha} \tau_1
      \rfn{\lam{\pi} v_\pi.1}
  \newrule{ShrRef-Split-Pair} \br[.2em]
  \xl \is{\xt} \refshr{\alpha} (\tau_0 \?+\? \tau_1) \rfn{\lam{\pi}\? \inj_i v_\pi}
    \ \yields\
    \xl \!+\! 1 \is{\xt} \refshr{\alpha} \tau_i \rfn{v}
  \newrule{ShrRef-Split-Sum}
\end{gather*}

\begin{proof}[Proof of \cref{rule:ShrRef-Split-Pair} and \cref{rule:ShrRef-Split-Sum}]
  Clear by definition.
\end{proof}

\subsubsection{Operations on ownership pointers}

We have the following lemmas for splitting and merging ownership pointers.
\begin{gather*}
  \xl \is{\xt} \rown_n (\tau_0 \?\X\? \tau_1) \rfn{v}
    \ \biyields\
    \xl \is{\xt} \rown_n \tau_0
      \rfn{\lam{\pi} v_\pi.0} \ \sep\
    \xl \!+\! \abs{\tau_0} \is{\xt} \rown_n \tau_1
      \rfn{\lam{\pi} v_\pi.1}
  \newrule{OwnPtr-Split-Merge-Pair} \br[-.1em]
  \xl \is{\xt} \rown_n (\tau_0 \?+\? \tau_1) \rfn{\lam{\pi}\? \inj_i v_\pi}
    \ \yields\
    \xl \mto i \ \sep\
    \xl \!+\! 1 \is{\xt} \rown_n \tau_i \rfn{v}
  \newrule{OwnPtr-Split-Sum}
\end{gather*}

\begin{proof}[Proof of \cref{rule:OwnPtr-Split-Merge-Pair} and \cref{rule:OwnPtr-Split-Sum}]
  Clear by definition.
\end{proof}
