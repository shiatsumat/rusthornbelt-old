From iris.algebra Require Import auth cmra functions gmap frac_agree.
From iris.proofmode Require Import tactics.
From iris.base_logic Require Import invariants.
From lrust.util Require Import util.
From lrust.prophecy Require Import base.

Open Scope proph_scope.

Implicit Type (q: Qp) (Ap Bp: ptType).
Implicit Type (x y: proph_var) (xs ys : proph_var_list) (π: proph_asn).

(** * Prophecy Equalizer *)

Definition proph_eqz `{!invG Σ, !prophG Σ} {A} (u v : _ → A) : iProp Σ :=
  ∀xs q, ⌜v ./ xs⌝ -∗ q:+[xs] ={↑prophN}=∗ ⟨π, u π = v π⟩ ∗ q:+[xs].

Notation "u :== v" := (proph_eqz u v)
  (at level 70, format "u  :==  v") : bi_scope.

Section lemmas.
Context `{!invG Σ, !prophG Σ}.

(** ** Constructing Prophecy Equalizers *)

Lemma proph_token_eqz x v : proph_ctx -∗ 1:[x] -∗ (.$ x) :== v.
Proof.
  iIntros "PROPH Tok". iIntros (???) "Ptoks".
  by iMod (proph_resolve with "PROPH Tok Ptoks").
Qed.

Lemma proph_obs_eqz {A} (u v : _ → A) : ⟨π, u π = v π⟩ -∗ u :== v.
Proof. iIntros "?". iIntros (???) "?". iModIntro. by iSplit. Qed.

Lemma proph_eqz_modify {A} (u u' v : _ → A) :
  ⟨π, u' π = u π⟩ -∗ u :== v -∗ u' :== v.
Proof.
  iIntros "#Obs Eqz". iIntros (???) "Ptoks".
  iMod ("Eqz" with "[%//] Ptoks") as "[#Obs' ?]". iModIntro. iSplitR; [|done].
  iDestruct (proph_obs_merge with "Obs Obs'") as "Obs''".
  by iApply proph_obs_weaken; [|iApply "Obs''"] => ? [-> ?].
Qed.

Lemma proph_eqz_constr {A B} f `{Inj A B (=) (=) f} u v :
  u :== v -∗ f ∘ u :== f ∘ v.
Proof.
  iIntros "Eqz". iIntros (?? Dep) "Ptoks". move/proph_dep_destr in Dep.
  iMod ("Eqz" with "[%//] Ptoks") as "[#Obs ?]". iModIntro. iSplitR; [|done].
  iApply proph_obs_weaken; [|by iApply "Obs"] => ??. by apply (f_equal f).
Qed.

Lemma proph_eqz_constr2 {A B C} f `{Inj2 A B C (=) (=) (=) f} u u' v v' :
  u :== v -∗ u' :== v' -∗ f ∘ u ⊛ u' :== f ∘ v ⊛ v'.
Proof.
  iIntros "Eqz Eqz'". iIntros (?? Dep) "Ptoks".
  move: Dep=> /proph_dep_destr2 [??].
  iMod ("Eqz" with "[%//] Ptoks") as "[#Obs Ptoks]".
  iMod ("Eqz'" with "[%//] Ptoks") as "[#Obs' ?]". iModIntro. iSplitR; [|done].
  iDestruct (proph_obs_merge with "Obs Obs'") as "Obs''".
  iApply proph_obs_weaken; [|by iApply "Obs''"] => ? [??].
  by apply (f_equal2 f).
Qed.

End lemmas.

(** * Unique Reference Camera *)

Local Definition itemR Ap := frac_agreeR (leibnizO ((proph_asn → Ap) * nat)).
Local Definition unqref_gmapUR Ap := gmapUR positive (itemR Ap).
Local Definition unqref_smryUR := discrete_funUR unqref_gmapUR.
Definition unqrefUR := authUR unqref_smryUR.

Implicit Type S: unqref_smryUR.

Local Definition item {Ap} q (vd: _ * _) : itemR Ap :=
  @to_frac_agree (leibnizO _) q vd.
Local Definition line x q vd : unqref_smryUR :=
  .{[x.ty := {[x.id := item q vd]}]}.
Local Definition add_line x q vd S : unqref_smryUR :=
  .<[x.ty := <[x.id := item q vd]> (S x.ty)]> S.

Definition unqrefΣ := #[GFunctor unqrefUR].
Class unqrefPreG Σ := UnqrefPreG { unqref_preG_inG :> inG Σ unqrefUR }.
Class unqrefG Σ := UnqrefG { unqref_inG :> unqrefPreG Σ; unqref_name : gname }.
Instance subG_unqrefPreG {Σ} : subG unqrefΣ Σ → unqrefPreG Σ.
Proof. solve_inG. Qed.

Definition unqrefN := nroot .@ "unqref".

(** * Iris Propositions *)

Section defs.
Context `{!invG Σ, !prophG Σ, !unqrefG Σ}.

(** Unique Reference Context *)
Definition unqref_inv: iProp Σ := ∃S, own unqref_name (● S).
Definition unqref_ctx: iProp Σ := inv unqrefN unqref_inv.

Local Definition own_line x q vd := own unqref_name (◯ line x q vd).

(** Value Observer *)
Definition val_obs x vd : iProp Σ := own_line x (1/2) vd.

(** Prophecy Control *)
Local Definition val_obs2 x vd : iProp Σ := own_line x 1 vd.
Definition proph_ctrl x vd : iProp Σ :=
  (val_obs x vd ∗ 1:[x]) ∨ ((∃vd', val_obs2 x vd') ∗ (.$ x) :== vd.1).

End defs.

Notation "VO[ x ] vd" := (val_obs x vd)
  (at level 5, format "VO[ x ]  vd") : bi_scope.
Notation "VO2[ x ] vd" := (val_obs2 x vd)
  (at level 5, format "VO2[ x ]  vd") : bi_scope.
Notation "PC[ x ] vd" := (proph_ctrl x vd)
  (at level 5, format "PC[ x ]  vd") : bi_scope.

(** * Lemmas *)

Section lemmas.
Context `{!invG Σ, !prophG Σ, !unqrefG Σ}.

Local Lemma own_line_agree x q q' vd vd' :
  own_line x q vd -∗ own_line x q' vd' -∗ ⌜(q + q' ≤ 1)%Qp ∧ vd = vd'⌝.
Proof.
  iIntros "Own Own'". iDestruct (own_valid_2 with "Own Own'") as %Val.
  iPureIntro. move: Val.
  rewrite -auth_frag_op auth_frag_valid discrete_fun_singleton_op
    discrete_fun_singleton_valid singleton_op singleton_valid.
  by move/frac_agree_op_valid.
Qed.

Local Lemma vo_vo2 x vd : VO[x] vd ∗ VO[x] vd ⊣⊢ VO2[x] vd.
Proof.
  by rewrite -own_op -auth_frag_op discrete_fun_singleton_op
    singleton_op /item -frac_agree_op Qp_half_half.
Qed.

Local Lemma vo_pc x vd :
  VO[x] vd -∗ PC[x] vd -∗ VO2[x] vd ∗ 1:[x].
Proof.
  iIntros "Vo Pc". iDestruct "Pc" as "[[? Tok]|[ExVo2 _]]"; last first.
  { iDestruct "ExVo2" as (?) "Vo2".
    by iDestruct (own_line_agree with "Vo Vo2") as %[? _]. }
  iSplitR "Tok"; [|done]. rewrite -vo_vo2. by iSplitL "Vo".
Qed.

(** Instances and Later *)

Global Instance unqref_ctx_persistent : Persistent unqref_ctx := _.

Global Instance val_obs_timeless x vd : Timeless (VO[x] vd) := _.

Lemma vo_later_pc x vd :
  VO[x] vd -∗ ▷ PC[x] vd -∗ ◇ (VO[x] vd ∗ PC[x] vd).
Proof.
  iIntros "Vo Pc". iDestruct "Pc" as "[> ?|[> ExVo2 _]]"; last first.
  { iDestruct "ExVo2" as (?) "Vo2".
    by iDestruct (own_line_agree with "Vo Vo2") as %[? _]. }
  iModIntro. iSplitL "Vo"; by [|iLeft].
Qed.

(** Initialization *)

Lemma unqref_init `{!unqrefPreG Σ} E :
  ↑unqrefN ⊆ E → ⊢ |={E}=> ∃ _: unqrefG Σ, unqref_ctx.
Proof.
  move=> ?. iMod (own_alloc (● ε)) as (γ) "Auth"; [by apply auth_auth_valid|].
  set IUnqrefG := UnqrefG Σ _ γ. iExists IUnqrefG.
  iMod (inv_alloc _ _ unqref_inv with "[Auth]") as "?"; by [iExists ε|].
Qed.

Definition pval_to_pt {A} (v: proph_asn → A) := PtType A (v inhabitant).
Notation "PT{ v }" := (pval_to_pt v)
  (at level 5, format "PT{ v }") : proph_scope.

Lemma unqref_intro {A} E (v: _ → A) d :
  ↑prophN ∪ ↑unqrefN ⊆ E → proph_ctx -∗ unqref_ctx ={E}=∗
    ∃i, let x := $(PT{v},i) in VO[x] (v,d) ∗ PC[x] (v,d).
Proof.
  iIntros (?) "PROPH ?". iInv unqrefN as (S) "> Auth".
  set Ap := PT{v}. set I := dom (gset _) (S Ap).
  iMod (proph_intro _ I with "PROPH") as (i NIn) "Tok"; [by solve_ndisj|].
  move: NIn=> /not_elem_of_dom ?.
  set x := $(Ap,i). set S' := add_line x 1 (v,d) S.
  iMod (own_update _ _ (● S' ⋅ ◯ line x 1 (v,d)) with "Auth") as "[? Vo2]".
  { by apply auth_update_alloc,
      discrete_fun_insert_local_update, alloc_singleton_local_update. }
  iModIntro. iSplitR "Vo2 Tok"; [by iExists S'|]. iModIntro. iExists i.
  iDestruct (vo_vo2 with "Vo2") as "[Vo Vo']".
  iSplitL "Vo"; [done|]. iLeft. by iSplitL "Vo'".
Qed.

Lemma unqref_agree x vd vd' : VO[x] vd -∗ PC[x] vd' -∗ ⌜vd = vd'⌝.
Proof.
  iIntros "Vo Pc". iDestruct "Pc" as "[[Own _]|[ExVo2 _]]";
    [|iDestruct "ExVo2" as (?) "Own"];
    by iDestruct (own_line_agree with "Vo Own") as %[??].
Qed.

Lemma unqref_proph_tok x vd :
  VO[x] vd -∗ PC[x] vd -∗ VO[x] vd ∗ 1:[x] ∗ (1:[x] -∗ PC[x] vd).
Proof.
  iIntros "Vo Pc". iDestruct (vo_pc with "Vo Pc") as "[Vo2 Tok]".
  iDestruct (vo_vo2 with "Vo2") as "[Vo Vo']". iSplitL "Vo"; [done|].
  iSplitL "Tok"; [done|]. iIntros "?". iLeft. by iSplitL "Vo'".
Qed.

Lemma unqref_update E x vd vd' : ↑unqrefN ⊆ E →
  unqref_ctx -∗ VO[x] vd -∗ PC[x] vd ={E}=∗ VO[x] vd' ∗ PC[x] vd'.
Proof.
  iIntros (?) "? Vo Pc". iDestruct (vo_pc with "Vo Pc") as "[Vo2 Tok]".
  iInv unqrefN as (S) "> Auth". set S' := add_line x 1 vd' S.
  iMod (own_update_2 _ _ _ (● S' ⋅ ◯ line x 1 vd') with "Auth Vo2")
    as "[? Vo2]".
  { apply auth_update, discrete_fun_singleton_local_update_any,
      singleton_local_update_any => ? _. by apply exclusive_local_update. }
  iModIntro. iSplitR "Vo2 Tok"; [by iExists S'|]. iModIntro.
  iDestruct (vo_vo2 with "Vo2") as "[Vo Vo']". iSplitL "Vo"; [done|].
  iLeft. by iSplitL "Vo'".
Qed.

Lemma unqref_resolve E x v d ys q : ↑prophN ⊆ E → v ./ ys →
  proph_ctx -∗ VO[x] (v,d) -∗ PC[x] (v,d) -∗ q:+[ys] ={E}=∗
    ⟨π, π x = v π⟩ ∗ PC[x] (v,d) ∗ q:+[ys].
Proof.
  iIntros (??) "PROPH Vo Pc Ptoks".
  iDestruct (vo_pc with "Vo Pc") as "[Vo2 Tok]".
  iMod (proph_resolve with "PROPH Tok Ptoks") as "[#Obs ?]"; [done|done|].
  iModIntro. iSplitR; [done|]. iSplitL "Vo2"; [|done].
  iRight. iSplitL; [by iExists (v,d)|]. by iApply proph_obs_eqz.
Qed.

Lemma unqref_preresolve E x u v d ys q : ↑prophN ⊆ E → u ./ ys →
  proph_ctx -∗ VO[x] (v,d) -∗ PC[x] (v,d) -∗ q:+[ys] ={E}=∗
    ⟨π, π x = u π⟩ ∗ q:+[ys] ∗ (∀v' d', u :== v' -∗ PC[x] (v',d')).
Proof.
  iIntros (??) "PROPH Vo Pc Ptoks".
  iDestruct (vo_pc with "Vo Pc") as "[Vo2 Tok]".
  iMod (proph_resolve with "PROPH Tok Ptoks") as "[#Obs Ptok]"; [done|done|].
  iModIntro. iSplitR; [done|]. iSplitL "Ptok"; [done|]. iIntros (??) "Eqz".
  iRight. iSplitR "Eqz"; [by iExists (v,d)|].
  by iDestruct (proph_eqz_modify with "Obs Eqz") as "?".
Qed.

Lemma proph_ctrl_eqz x v d : proph_ctx -∗ PC[x] (v,d) -∗ (.$ x) :== v.
Proof.
  iIntros "#? Pc".
  iDestruct "Pc" as "[[_ ?]|[_ ?]]"; by [iApply proph_token_eqz|].
Qed.

End lemmas.
